            <!-- Right side column. Contains the navbar and content of the page -->
            <!-- DataTables CSS -->

            <aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Meedar Real Estate
                        <small>Admin Panel</small>
                    </h1>
                    
                </section>

                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <!-- left column -->
                        <div class="col-md-6" style="width:100%;">
                            <!-- general form elements -->
                            <div class="box box-primary" style="overflow-x:scroll; ">
                                <div class="box-header">
                                    <h3 class="box-title">Rental Properties</h3>
                                </div><!-- /.box-header -->
                                    <table id="example" border="1" style="margin-left:1%; " class="display" cellspacing="0" width="98%">
                                        <thead>
                                            <tr>
                                                <th>Actions</th>
                                                <th>Property ID</th>
                                                <th>City</th>
                                                <th>Country</th>
                                                <th>Location</th>
                                                <th>Developer</th>
                                                <th>Community</th>
                                                <th>Area</th>
                                                <th>Value</th>
                                                <th>Reference Document</th>
                                                <th>Title Deed</th>
                                                <th>Owner</th>
                                                <th>Customer</th>
                                                <th>Agent ID</th>
                                                <th>Active Agent</th>
                                                
                                            </tr>
                                        </thead>
                                     
                                        <tbody>
                                            <?php 

                                                $link= base_url()."index.php/admin/Properties/EditProp/";

                                                foreach($properties as $property){
                                                    echo '<tr>';
                                                    echo '<td><button name="'.$property->property_id.'"><a href="'.$link.$property->property_id.'">Edit</a></button></td>';
                                                    echo '<td>'.$property->property_id.'</td>';
                                                    echo '<td>'.$property->city.'</td>';
                                                    echo '<td>'.$property->country.'</td>';
                                                    echo '<td>'.$property->location.'</td>';
                                                    echo '<td>'.$property->developer.'</td>';
                                                    echo '<td>'.$property->community.'</td>';
                                                    echo '<td>'.$property->area.'</td>';
                                                    echo '<td>'.$property->value.'</td>';
                                                    echo '<td>'.$property->reference_document.'</td>';
                                                    echo '<td>'.$property->title_deed.'</td>';
                                                    echo '<td>'.$property->owner.'</td>';
                                                    echo '<td>'.$property->customer.'</td>';
                                                    echo '<td>'.$property->agent_id.'</td>';
                                                    echo '<td>'.$property->active_agent.'</td>';
                                                    echo '</tr>';
                                                }
                                            ?>                                           
                                        </tbody>
                                    </table>
                               
                            </div><!-- /.box -->

                            
                        </div><!--/.col (right) -->
                    </div>   <!-- /.row -->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
            
          
        <!-- DataTables -->
        <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.js"></script>
            <script type="text/javascript">
             $(document).ready(function() {
                    $('#example').DataTable();
                } );
             </script>