<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('index');
	}
	public function send(){

				$email_to="info@sooma.ae";
				$email_subject="Main Website Enquires";
				$email_message=$this->input->post('text');
				$name= str_replace(' ', '', $this->input->post('name'));
				$headers = "From: ".$name."\r\n".
				"Reply-To: ".$this->input->post('email')."\r\n'" .
				"X-Mailer: PHP/" . phpversion();
				if(mail($email_to, $email_subject, $email_message, $headers)){ 
				echo "mail sent!";
				echo "<script>setTimeout(\"location.href = '".base_url()."';\",300);</script>";
			}
			else{
				echo "<script> alert('There was an error sending your message please try again');</script>";
				echo "<script>setTimeout(\"location.href = '".base_url()."';\",500);</script>";

			}

        }

        public function send2(){
        		 $name = str_replace(' ', '', $_FILES['file']['name']);
            rename($_FILES['file']['tmp_name'], 'assets/files/'.$name);
            chmod($_SERVER['DOCUMENT_ROOT'] .'/Sooma/Sooma/assets/files/'.$name,0777);

				$email_to="info@sooma.ae";
				$email_subject="Main Website Enquires";
				$email_message=$this->input->post('text')."\r\n  Download CV HERE: ".base_url().'assets/files/'.$name;
				$name= str_replace(' ', '', $this->input->post('name'));
				$headers = "From: ".$name."\r\n".
				"Reply-To: \r\n'" .
				"X-Mailer: PHP/" . phpversion();
				if(mail($email_to, $email_subject, $email_message, $headers)){ 
				echo "mail sent!";
				echo "<script>setTimeout(\"location.href = '".base_url()."';\",300);</script>";
			}
			else{
				echo "<script> alert('There was an error sending your message please try again');</script>";
				echo "<script>setTimeout(\"location.href = '".base_url()."';\",500);</script>";

			}

        }
        
        public function sendCV(){
            $this->load->library('email');

            $this->email->from('ahmed@creadevz.com.com', 'Ahmed Saad');
            $this->email->to('fady.fawzy19@hotmail.com'); 
            //$this->email->cc('another@another-example.com'); 
            //$this->email->bcc('them@their-example.com'); 

            $this->email->subject('Job Application');
            
            $name = str_replace(' ', '', $_FILES['file']['name']);
            rename($_FILES['file']['tmp_name'], 'assets/files/'.$name);
            chmod($_SERVER['DOCUMENT_ROOT'] .'/Sooma/Sooma/assets/files/'.$name,0777);
            
            //$this->email->attach('/assets/files/'.$_FILES['file']['name']);
            $msg = 'Name: '.$this->input->post('name').'\n'.
                   'Email: '.$this->input->post('email').'\n'.
                   'Additional Info: '.$this->input->post('text').'\n'.
                   'CV: '.$_SERVER['DOCUMENT_ROOT'] .'/Sooma/Sooma/assets/files/'.$name;
            $this->email->message($msg);	

            $this->email->send();

            print_r($this->email->print_debugger());
        }
        
        public function download($file){
            $viewdata['url'] = 'http://www.creadevz.com/Sooma/Sooma/'.$file;
            $viewdata['redirect'] = 'Welcome';
            $this->load->view('empty',$viewdata);
        }
}
