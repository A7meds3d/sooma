<!DOCTYPE html>
<!--[if lt IE 9 ]><html class="ie ie-lt9 no-js" lang="en"><![endif]-->
<!--[if IE 9 ]><html class="ie ie9 no-js" lang="en"><![endif]-->
<!--[if gt IE 9 | !IE]><!-->
<html class="no-js fixed" lang="en">
<!--<![endif]-->
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="description" content="">
<meta name="keywords" content="">
<meta name="author" content="">
<title>SOOMA</title>
<link rel='stylesheet' href='<?php echo base_url();?>assets/css/bootstrap.min.css'>
<link rel='stylesheet' href='<?php echo base_url();?>assets/css/style.css'>
<link rel='stylesheet' href='<?php echo base_url();?>assets/css/color.css'>
<link rel='stylesheet' href='<?php echo base_url();?>assets/css/title-size.css'>
<link rel='stylesheet' href='<?php echo base_url();?>assets/css/custom.css'>
<link rel="icon" href="<?php echo base_url();?>assets/img/favicon.ico">
</head>
<body>

  <!-- loader -->
  <div id="site-loader">
    <div class="loader"></div>
  </div>
  <!-- loader -->

  <!-- site wrap -->
  <div id="site-wrap">

    <!-- backgeound -->
    <div id="bg">
      <div id="img"></div>
      <div id="video"></div>
      <div id="overlay"></div>
      <div id="effect">
        <img src="<?php echo base_url();?>assets/img/bg/cloud-01.png" alt="" id="cloud1">
        <img src="<?php echo base_url();?>assets/img/bg/cloud-02.png" alt="" id="cloud2">
        <img src="<?php echo base_url();?>assets/img/bg/cloud-03.png" alt="" id="cloud3">
        <img src="<?php echo base_url();?>assets/img/bg/cloud-04.png" alt="" id="cloud4">
      </div>
      <!--<canvas id="js-canvas"></canvas>-->
    </div>
    <!-- /background -->

    <!-- subscribe -->
    <div id="form">
      <div id="subscribe">
        <div class="tb-cell">
          <p class="animation section-subtitle">Subscribe to get notified.</p>
          <h2 class="section-title">Newsletter</h2>
          <!-- subscribe form -->
          <form id="form-subscribe" class="form-lg container">
            <input type="text" name="email" class="form-control" placeholder="Email address">
            <button type="submit">
              <i class="ion-email"></i>
            </button>
          </form>
          <!-- /subscribe form -->
        </div>
      </div>
    </div>
    <!-- /subscribe -->

    <!-- site main -->
    <main id="site-main">
      <!-- home -->
      <section id="home" >
        <div class="section-wrap">
          <div class="section-cell">
            <img class="header-logoo logo-light" src="<?php echo base_url();?>assets/img/bg/logo.png" align="center" style="    margin: 0px auto 0px auto;"  alt="">
            <h2 align="center" style="    margin: 0px auto 0px auto;" > COMING SOON </h2>

              
            <div class="container">
              <!-- section header -->

              <div class=" row text-center" style="margin-top:1%">
                <div class="col-xs-12" align="center" style="display-inline">

                  <Span class="btn btn-alt btn-lg btn-primary" style="margin-right:2%;width:10%;" ><a href="#Career" data-toggle="modal" class="enable-tooltip" data-placement="bottom" title="Settings" class="btn btn-alt btn-primary"><i class=""></i>Careers</a>
                  </span>
                

                <Span class="btn btn-alt btn-lg btn-primary" style="width:10%" ><a  href="#Newsletter" data-toggle="modal" class="enable-tooltip" data-placement="bottom" title="Settings" class="btn btn-alt btn-primary"><i class=""></i>Contact Us</a>
                  </span>

                </div>
                <div class="col-xs-12">

                  <div class="animation section-divider"></div>
                </div>
              </div>
              <!-- /setion header -->

              <!-- section main -->
              <div class="section-main row">
                <div class="col-xs-12">
                  <!-- countdown -->
                  <div id="countdown" class="animation-04">
                    <div class="row">
                      <div class="col-xs-3 col-countdown">
                        <div class="countdown-section">
                          <div class="countdown-amount days"></div>
                          <div class="countdown-period days_ref">Days</div>
                        </div>
                      </div>
                      <div class="col-xs-3 col-countdown">
                        <div class="countdown-section">
                          <div class="countdown-amount hours"></div>
                          <div class="countdown-period hours_ref">Hours</div>
                        </div>
                      </div>
                      <div class="col-xs-3 col-countdown">
                        <div class="countdown-section">
                          <div class="countdown-amount minutes"></div>
                          <div class="countdown-period minutes_ref">Minutes</div>
                        </div>
                      </div>
                      <div class="col-xs-3 col-countdown">
                        <div class="countdown-section">
                          <div class="countdown-amount seconds"></div>
                          <div class="countdown-period seconds_ref">Seconds</div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- /countdown -->
                </div>
              </div>
              <!-- /setion main -->
            </div>
          </div>
        </div>
      </section>
      <!-- /home -->

    </main>
    <!-- /site main -->

    <!-- site footer -->
    <footer id="site-footer">
      <!-- footer social -->
      <a href="#" id="volume">
        <span></span>
        <span></span>
        <span></span>
        <span></span>
      </a>
      <div id="footer-social">
        <a href="#" title="" target="_blank"><i class="ion-social-twitter"></i></a>
        <a href="#" title="" target="_blank"><i class="ion-social-googleplus"></i></a>
        <a href="#" title="" target="_blank"><i class="ion-social-instagram-outline"></i></a>
        <a href="#" title="" target="_blank"><i class="ion-social-facebook"></i></a>
      </div>
      <!-- /footer social -->
    </footer>
    <!-- /site footer -->

    <!-- audio -->
    <audio id="audio-player" loop>
      <source src="<?php echo base_url();?>assets/audio/int.mp3" type="audio/mpeg">
    </audio>
    <!-- audio -->
  </div>
  <!-- /site wrap -->

   <!-- User Settings, modal which opens from Settings link (found in top right user menu) and the Cog link (found in sidebar user info) -->
        <div id="Career" style="color:black;" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <!-- Modal Header -->
                    <div class="modal-header text-center">
                        <h2 class="modal-title"><i class="fa fa-pencil"></i> Upload your CV</h2>
                    </div>
                    <!-- END Modal Header -->

                    <!-- Modal Body -->
                    <div class="modal-body" >
                        <form action="<?php echo base_url()?>/index.php/Welcome/send2" method="post" enctype="multipart/form-data" class="form-horizontal form-bordered " >
                            <fieldset>
                                <legend>Upload your CV</legend>
                                <div class="form-group">
                                    <div class="col-md-12">
                                      <input type="text" id="name" name="name" placeholder="Full Name" style="width:100%;padding-left:5px;">
                                    </div>
                                </div>
                                
                                <div class="form-group">  
                                    <div class="col-md-12">
                                        <input type="file" name="file" style="width:100%;padding-left:5px;">
                                    </div>
                                </div>
                                 <div class="form-group">
                                    <div class="col-md-12">
                                      <textarea id="text" name="text" placeholder="Add your message or any additional info here" style="width:100%;padding-left:5px;"></textarea>
                                    </div>
                                </div>
                               
                            </fieldset>
                              
                            <div class="form-group form-actions">
                                <div class="col-xs-12 text-right">
                                    <button type="button" class="btn btn-lg btn-default" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-lg btn-primary">Save Changes</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- END Modal Body -->
                </div>
            </div>
        </div>
        <!-- END User Settings -->

           <!-- User Settings, modal which opens from Settings link (found in top right user menu) and the Cog link (found in sidebar user info) -->
        <div id="Newsletter" style="color:black;" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <!-- Modal Header -->
                    <div class="modal-header text-center">
                        <h2 class="modal-title"><i class="fa fa-pencil"></i> Contact Form</h2>
                    </div>
                    <!-- END Modal Header -->

                    <!-- Modal Body -->
                    <div class="modal-body" >
                        <form action="<?php echo base_url();?>index.php/Welcome/send" enctype="multipart/form-data" method="post"  class="form-horizontal form-bordered " >
                            <fieldset>
                                <legend>Contact us</legend>
                                <div class="form-group">
                                    <div class="col-md-12">
                                      <input type="text" id="name" name="name" placeholder="Full Name" style="width:100%;padding-left:5px;">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12">
                                      <input type="text" id="email" name="email" placeholder="Your Email" style="width:100%;padding-left:5px;">
                                    </div>
                                </div>
                               
                                <div class="form-group">
                                    <div class="col-md-12">
                                      <textarea id="text" name="text" placeholder="type your message here" style="width:100%;padding-left:5px;"></textarea>
                                    </div>
                                </div>
                               
                            </fieldset>
                              
                            <div class="form-group form-actions">
                                <div class="col-xs-12 text-right">
                                    <button type="button" class="btn btn-lg btn-default" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-lg btn-primary">Send Message</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- END Modal Body -->
                </div>
            </div>
        </div>
        <!-- END User Settings -->



  <!-- script -->
  <script src='<?php echo base_url();?>assets/js/vendor/jquery-2.1.4.min.js'></script>
  <!--[if lte IE 9]><!-->
  <script src='<?php echo base_url();?>assets/js/vendor/html5shiv.min.js'></script>
  <!--<![endif]-->
  <script src='<?php echo base_url();?>assets/js/vendor/bootstrap.min.js'></script>
  <script src='<?php echo base_url();?>assets/js/vendor/vendor.js'></script>
  <script src='<?php echo base_url();?>assets/js/variable.js'></script>
  <script src='<?php echo base_url();?>assets/js/main.js'></script>
  <!-- /script -->

</body>
</html>