
<html>
    <head>
    </head>
    <body>
        <input type="hidden" id="url" value="<?php echo $url;?>">
        <script src="<?php echo base_url(); ?>assets/js/jquery-2.1.4.min.js"></script>
        <script>
            downloadFile($('#url').val())
            function downloadFile(url)
            {
                var iframe;
                iframe = document.getElementById("download-container");
                if (iframe === null)
                {
                    iframe = document.createElement('iframe');  
                    iframe.id = "download-container";
                    iframe.style.visibility = 'hidden';
                    document.body.appendChild(iframe);
                }
                iframe.src = url;   
            }
            
            $(document).ready(function(){
                setTimeout(function(){
                    window.location.href = '<?php echo base_url(); ?>index.php/<?php echo $redirect?>/'
                },1000);
            });
            
        </script>
        
    </body>
    
</html>
