<!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Meedar Real Estate
                        <small>Admin Panel</small>
                    </h1>
                    
                </section>

                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <!-- left column -->
                        <div class="col-md-6">
                            <!-- general form elements -->
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title">Edit Lead</h3>
                                </div><!-- /.box-header -->

                                <!-- form start -->
                                <form role="form" method="POST" action="<?php echo base_url(); ?>index.php/Admin/Leads/EditLead">
                                   

                                        <div class="form-group">
                                            <label >Lead ID</label>
                                            <input  value="<?php echo $leads->lead_id ?>" type="text"  placeholder="ID" class="form-control" id="lead_id" name="lead_id" readonly >
                                        </div>

                                        <div class="form-group">
                                            <label>Source</label>
                                            <input value="<?php echo $leads->source ?>" type="text" class="form-control" id="source" name="source" placeholder="Source" required>
                                        </div>

                                        <div class="form-group">
                                            <label>Type</label>
                                            <input value="<?php echo $leads->type ?>" type="text" class="form-control" id="type" name="type" placeholder="country" required>
                                        </div>

                                        <div class="form-group">
                                            <label >First Name</label>
                                            <input value="<?php echo $leads->first_name ?>" type="text" class="form-control" id="first_name" name="first_name" placeholder="First Name" required>
                                        </div>

                                        <div class="form-group">
                                            <label >Last Name</label>
                                            <input value="<?php echo $leads->last_name ?>" type="text" class="form-control" id="last_name" name="last_name" placeholder="Last Name" required>
                                        </div>

                                        <div class="form-group">
                                            <label >Nationality</label>
                                            <input value="<?php echo $leads->nationality ?>" type="text" class="form-control" id="nationality" name="nationality" placeholder="Nationality" required>
                                        </div>

                                        <div class="form-group">
                                            <label >Address</label>
                                            <input value="<?php echo $leads->address ?>" type="text" class="form-control" id="address" name="address" placeholder="Address" required>
                                        </div>

                                        <div class="form-group">
                                            <label >Post Box</label>
                                            <input value="<?php echo $leads->post_box ?>"type="number" class="form-control" id="post_box" name="post_box" placeholder="Post Box" required>
                                        </div>

                                        <div class="form-group">
                                            <label >City </label>
                                            <input value="<?php echo $leads->city ?>" type="text" class="form-control" id="city" name="city" placeholder="City" required>
                                        </div>


                                        <div class="form-group">
                                            <label >Country</label>
                                            <input value="<?php echo $leads->country ?>" type="text" class="form-control" id="country" name="country" placeholder="Country" >
                                        </div>

                                        <div class="form-group">
                                            <label >Phone 1</label>
                                            <input value="<?php echo $leads->phone_1 ?>" type="text" class="form-control" placeholder="Phone 1" id="phone_1" name="phone_1">
                                        </div>

                                        <div class="form-group">
                                            <label >Phone 2</label>
                                            <input value="<?php echo $leads->phone_2 ?>" type="text" class="form-control" id="phone_2" name="phone_2" placeholder="Phone 2" >
                                        </div>

                                        <div class="form-group">
                                            <label >Phone 3</label>
                                            <input value="<?php echo $leads->phone_3 ?>" type="number" class="form-control" id="phone_3" name="phone_3" placeholder="Phone 3" >
                                        </div>

                                        <div class="form-group">
                                            <label >Mobile</label>
                                            <input value="<?php echo $leads->mobile ?>" type="text" class="form-control" id="mobile" name="mobile" placeholder="Mobile" >
                                        </div>
                                        <div class="form-group">
                                            <label >Email</label>
                                            <input value="<?php echo $leads->email ?>" type="email" class="form-control" id="email" name="email" placeholder="Email" >
                                        </div>
                                        
                                    </div><!-- /.box-body -->

                                    <div class="box-footer ">
                                        <button type="submit" class="btn btn-primary">Update</button>
                                    </div>
                                </form>
                            </div><!-- /.box -->

                            
                        </div><!--/.col (right) -->
                    </div>   <!-- /.row -->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
