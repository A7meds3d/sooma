var ContactUs = function () {

    return {
        //main function to initiate the module
        init: function () {
			var map;
			$(document).ready(function(){
			  map = new GMaps({
				div: '#map',
	            lat: 25.184003,
				lng: 55.260919,
			  });
			   var marker = map.addMarker({
		            lat: 25.184003,
					lng: 55.260919,
		            title: 'Meedar Real Estate',
		            infoWindow: {
		                content: "<b>Meedar Real Estate</b><br> 3910, Citadel Tower, Business Bay,<br> Dubai, UAE. <br> PO Box 214966"
		            }
		        });

			   marker.infoWindow.open(map, marker);
			});
        }
    };

}();

