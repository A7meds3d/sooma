
			
			<!-- contain main informative part of the site -->
			<main id="main" role="main">
                <!-- page banner -->
				<header class="page-banner">
					<div class="stretch">
						<img alt="image description" src="<?php echo base_url();?>Assets/assets/Services2.jpg" style="width: 2139px; height: 445.625px; margin-top: -22.8125px; margin-left: 0px;">
					</div>
					<div class="container">
						<div class="row">
							<div class="col-xs-12">
								<div class="holder">
									<h1 class="heading text-capitalize">Live Gallery</h1>
									<p>Your challenge is our progress</p>
								</div>
								<ul class="breadcrumbs list-inline">
									<li><a href="#">HOME</a></li>
									<li class="active"><a href="#">Live Gallery</a></li>
								</ul>
							</div>
						</div>
					</div>
				</header>
				
				<!-- work section -->
				<section class="work-section padding-top-90 padding-bottom-90">
                    <!-- isotop controls -->
                    <ul id="work-filter" class="list-inline isotop-controls">
                        <li class="active"><a href="#">All</a></li>
                        <li><a data-filter=".work-design" href="#">Villa</a></li>
                        <li><a data-filter=".work-fashion" href="#">Twin House</a></li>
                        <li><a data-filter=".work-illustration" href="#">Al Fouad</a></li>
                        <li><a data-filter=".work-products" href="#">Buildings</a></li>
                    </ul>
					<div class="container padding-bottom-60">
						<div class="row">
							<div class="col-xs-12">
								<div class="icotop-holder" id="work" style="position: relative; height: 2885px;">
                                    <!-- portfolio block coll-2 style6 -->
                                    <div class="portfolio-block coll-2 style6 work-design" >
                                        <!-- box -->
                                        <div class="box">
                                            <div class="img-box">
                                                <img src="<?php echo base_url();?>Assets/fouad.jpg" alt="image description" class="img-responsive">
                                                <div class="over">
                                                    <a class="search lightbox" href="<?php echo base_url();?>Assets/fouad.jpg"><i class="fa fa-search"></i></a>
                                                    <a class="link" href="#"><i class="fa fa-link"></i></a>
                                                </div>
                                            </div>
                                            <div class="text-box">
                                                <h2 class="title text-left no-bg padding-zero margin-zero"><a href="http://creative-wp.com/demos/fekra/portfolio-masonry-2column.html#">Modern Villa </a></h2>
                                                <p class="text-left text-uppercase"><a href="#">Twin House Compound</a></p>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- portfolio block coll-2 style6 -->
                                    <div class="portfolio-block coll-2 style6 work-fashion" >
                                        <!-- box -->
                                        <div class="box">
                                            <div class="img-box">
                                                <img src="<?php echo base_url();?>Assets/fouad.jpg" alt="image description" class="img-responsive">
                                                <div class="over">
                                                    <a class="search lightbox" href="<?php echo base_url();?>Assets/fouad.jpg"><i class="fa fa-search"></i></a>
                                                    <a class="link" href="#"><i class="fa fa-link"></i></a>
                                                </div>
                                            </div>
                                            <div class="text-box">
                                                <h2 class="title text-left no-bg padding-zero margin-zero"><a href="http://creative-wp.com/demos/fekra/portfolio-masonry-2column.html#">Modern Villa </a></h2>
                                                <p class="text-left text-uppercase"><a href="#">Twin House Compound</a></p>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- portfolio block coll-2 style6 -->
                                    <div class="portfolio-block coll-2 style6 work-fashion" >
                                        <!-- box -->
                                        <div class="box">
                                            <div class="img-box">
                                                <img src="<?php echo base_url();?>Assets/fouad.jpg" alt="image description" class="img-responsive">
                                                <div class="over">
                                                    <a class="search lightbox" href="<?php echo base_url();?>Assets/fouad.jpg"><i class="fa fa-search"></i></a>
                                                    <a class="link" href="#"><i class="fa fa-link"></i></a>
                                                </div>
                                            </div>
                                            <div class="text-box">
                                                <h2 class="title text-left no-bg padding-zero margin-zero"><a href="http://creative-wp.com/demos/fekra/portfolio-masonry-2column.html#">Modern Villa </a></h2>
                                                <p class="text-left text-uppercase"><a href="#">Twin House Compound</a></p>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- portfolio block coll-2 style6 -->
                                    <div class="portfolio-block coll-2 style6 work-design" >
                                        <!-- box -->
                                        <div class="box">
                                            <div class="img-box">
                                                <img src="<?php echo base_url();?>Assets/fouad.jpg" alt="image description" class="img-responsive">
                                                <div class="over">
                                                    <a class="search lightbox" href="<?php echo base_url();?>Assets/fouad.jpg"><i class="fa fa-search"></i></a>
                                                    <a class="link" href="#"><i class="fa fa-link"></i></a>
                                                </div>
                                            </div>
                                            <div class="text-box">
                                                <h2 class="title text-left no-bg padding-zero margin-zero"><a href="http://creative-wp.com/demos/fekra/portfolio-masonry-2column.html#">Modern Villa </a></h2>
                                                <p class="text-left text-uppercase"><a href="#">Twin House Compound</a></p>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- portfolio block coll-2 style6 -->
                                    <div class="portfolio-block coll-2 style6 work-products" >
                                        <!-- box -->
                                        <div class="box">
                                            <div class="img-box">
                                                <img src="<?php echo base_url();?>Assets/fouad.jpg" alt="image description" class="img-responsive">
                                                <div class="over">
                                                    <a class="search lightbox" href="<?php echo base_url();?>Assets/fouad.jpg"><i class="fa fa-search"></i></a>
                                                    <a class="link" href="#"><i class="fa fa-link"></i></a>
                                                </div>
                                            </div>
                                            <div class="text-box">
                                                <h2 class="title text-left no-bg padding-zero margin-zero"><a href="http://creative-wp.com/demos/fekra/portfolio-masonry-2column.html#">Modern Villa </a></h2>
                                                <p class="text-left text-uppercase"><a href="#">Twin House Compound</a></p>
                                            </div>
                                        </div>
                                    </div>
                                    
								</div>
							</div>
						</div>
					</div>
					
				</section>

			</main>
			
 