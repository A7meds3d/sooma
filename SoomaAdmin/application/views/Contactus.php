
			
			<!-- contain main informative part of the site -->
			<main id="main" role="main">
                <!-- page banner -->
				<header class="page-banner">
					<div class="stretch">
						<img alt="image description" src="<?php echo base_url();?>Assets/assets/img01.jpg" style="width: 2139px; height: 445.625px; margin-top: -22.8125px; margin-left: 0px;">
					</div>
					<div class="container">
						<div class="row">
							<div class="col-xs-12">
								<div class="holder">
									<h1 class="heading text-capitalize">Contact us</h1>
									<p>Your challenge is our progress</p>
								</div>
								<ul class="breadcrumbs list-inline">
									<li><a href="#">HOME</a></li>
									<li class="active"><a href="#">Contact us</a></li>
								</ul>
							</div>
						</div>
					</div>
				</header>
				


                <!-- contact block -->
				<div class="contact-block container">
                    <!-- contact map -->
					<div class="row contact-map">
						<div class="col-xs-12">
							<div id="map" class="map"></div>
							 <script>
      function initMapp() {
        var mapDiv = document.getElementById('map');
        var map = new google.maps.Map(mapDiv);
      
      }
    </script>
    <script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBmVkDu1pFP8pkzXP4ucugwkcHR7olj_Go&callback=initMapp">
    </script>
						</div>
					</div>
                    <!-- contact message -->
					<div class="row contact-message">
						<div class="col-sm-6 col-xs-12">
							<h2>Send a message</h2>
							<p>Feel free to ask us about anything</p>
							<form action="<?php echo base_url();?>index.php/Home/send" class="contact-form2">
								<fieldset>
									<input class="form-control" id="name" name="name" type="text" placeholder="Name*">
									<input class="form-control" id="email" name="email" type="email" placeholder="E-mail *">
									<input class="form-control" id="phone" name="phone" type="text" placeholder="Website*">
									<textarea class="form-control" id="text" name="text" cols="30" rows="10" placeholder="Message*"></textarea>
									<input class="btn btn-f-info" type="submit" value="submit">
								</fieldset>
							</form>
						</div>
						<div class="col-sm-6 col-xs-12">
							<h2>How to reach us</h2>
							<p>Your are always welcome to contact us through the following</p>
							<div class="contact-address">
								<div class="alignleft">
									<img class="img-responsive" alt="image description" style="width:20%;float:left;" src="<?php echo base_url();?>Assets/assets/Logo1B.png">
								</div>
								<div class="contact-info"style="float:right;margin-top:-28%;"> 
									<h3><i class="fa fa-road"></i>Address</h3>
									<address><?php echo $location[0]->data;?></address>
									<h3><i class="fa fa-phone"></i>Phones Numbers</h3>
									<div class="tel-box">
										<span class="tel">
											<?php echo $phone[0]->data;?>
										</span>
										
									</div>
									<h3><i class="fa fa-envelope"></i>E-mail</h3>
									<a href="mailto:<?php echo $email[0]->data;?>"><?php echo $email[0]->data;?></a>
								</div>
							</div>
						</div>
					</div>
				</div>
			


				
				
			</main>
 