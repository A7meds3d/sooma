

			<!-- contain main informative part of the site -->
			<main id="main">
				<!-- app mainbanner -->
				<section class="app-mainbanner">
					<div class="container">
						<div class="row">
							<div class="col-xs-12 holder">
								<h1>Search Now<br>for your <span class="add">Future Home</span></h1>
								
								<form action="#" class="signup-form">
									<fieldset>
										<div class="frame"  style="    margin-bottom: -10px;">
											<select class="Selects" name="type">
												<option value="null">Any Emirate</option>
		                                        <option value="0">Dubai</option>
		                                        <option value="1">Sharqa</option>
		                                        <option value="2">El Ain</option>
		                                    </select>
											<select  class="Selects" name="type">
												<option value="null">Any Location</option>
		                                        <option value="0">Dubai</option>
		                                        <option value="1">Sharqa</option>
		                                        <option value="2">El Ain</option>
		                                    </select>
		                                    <select class="Selects" name="type">
												<option value="null">Any Type</option>
		                                        <option value="0">Rental</option>
		                                        <option value="1">Sale</option>
		                                        
		                                    </select>
											<select  class="Selects" name="type">
												<option value="null">Any Sub Location</option>
		                                        <option value="0">Dubai</option>
		                                        <option value="1">Sharqa</option>
		                                        <option value="2">El Ain</option>
		                                    </select>
											
										</div>
										<div class="frame">
											<select class="Selects" name="type">
												<option value="null">Min Price</option>
		                                        <option value="0">Dubai</option>
		                                        <option value="1">Sharqa</option>
		                                        <option value="2">El Ain</option>
		                                    </select>
											<select  class="Selects" name="type">
												<option value="null">Max Price</option>
		                                        <option value="0">Dubai</option>
		                                        <option value="1">Sharqa</option>
		                                        <option value="2">El Ain</option>
		                                    </select>
		                                    <select class="Selects" name="type">
												<option value="null">No Bedrooms</option>
		                                        <option value="0">Rental</option>
		                                        <option value="1">Sale</option>
		                                        
		                                    </select>
											<select  class="Selects" name="type">
												<option value="null">No Bathrooms</option>
		                                        <option value="0">Dubai</option>
		                                        <option value="1">Sharqa</option>
		                                        <option value="2">El Ain</option>
		                                    </select>
											
										</div>
										<button class="btn btn-submit">SEARCH For your Future Home</button>
									</fieldset>
								</form>
							</div>
						</div>
					</div>
					<div class="stretch">
						<img alt="image description" src="<?php echo base_url();?>Assets/assets/Services2.jpg" data-animate="fadeInUpRight" data-delay="300" class="fadeInUpRight animated" style="width: 1899px; height: 1186.88px; margin-top: -243.438px; margin-left: 0px;">
					</div>
				</section>
				<!-- section -->
				<section class="container padding-top-90 padding-bottom-90 " id="section1">
					<div class="row">
						<div class="col-md-12 col-xs-12">
                            <!-- page heading -->
                            <header class="page-heading left-align" style="margin:0px 0 30px;">
                                <div class="col-xs-12 col-sm-12">
                                <h2 class="lime text-capitalize font-medium margin-bottom-20">Properties Overview</h2>
                                <p>Ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam</p>
                                </div>
                            </header>
                        </div>

						
					</div>
					<div class="row realestate-services ">
						<div class="col-md-12 col-xs-12">
							<div class="box ">
								<div class="img-box">
									<div class="over">
										<div class="frame">
											<div class="block">
												<a href="portfolio-single-image.html" class="view" tabindex="0"><i class="fa fa-eye"></i></a>
												<a href="http://creative-wp.com/demos/fekra/images/real-estate/img05.jpg" class="expand lightbox" tabindex="0"><i class="fa fa-expand"></i></a>
												</div>
											</div>
										</div>
										<img src="http://creative-wp.com/demos/fekra/images/real-estate/img05.jpg" alt="image description">
									</div>
									<div class="text-box">
										<div class="txt">
										<h2>Akaarat Gadeda</h2>
										<p>Cairo <br>town house</p>
									</div>
									<div class="txt2">
										<ul class="star-list">
											<li class="active"><a href="#" tabindex="0"><i class="fa fa-star"></i></a></li>
											<li class="active"><a href="#" tabindex="0"><i class="fa fa-star"></i></a></li>
											<li class="active"><a href="#" tabindex="0"><i class="fa fa-star"></i></a></li>
											<li><a href="#" tabindex="0"><i class="fa fa-star"></i></a></li>
											<li><a href="#" tabindex="0"><i class="fa fa-star"></i></a></li>
										</ul>
										<span class="sale">FOR SALE</span>
									</div>
									<span class="price"><span class="add">$</span>32.000</span>
								</div>
							</div>
							<div class="box ">
								<div class="img-box">
									<div class="over">
										<div class="frame">
											<div class="block">
												<a href="portfolio-single-image.html" class="view" tabindex="0"><i class="fa fa-eye"></i></a>
												<a href="http://creative-wp.com/demos/fekra/images/real-estate/img05.jpg" class="expand lightbox" tabindex="0"><i class="fa fa-expand"></i></a>
												</div>
											</div>
										</div>
										<img src="http://creative-wp.com/demos/fekra/images/real-estate/img05.jpg" alt="image description">
									</div>
									<div class="text-box">
										<div class="txt">
										<h2>Akaarat Gadeda</h2>
										<p>Cairo <br>town house</p>
									</div>
									<div class="txt2">
										<ul class="star-list">
											<li class="active"><a href="#" tabindex="0"><i class="fa fa-star"></i></a></li>
											<li class="active"><a href="#" tabindex="0"><i class="fa fa-star"></i></a></li>
											<li class="active"><a href="#" tabindex="0"><i class="fa fa-star"></i></a></li>
											<li><a href="#" tabindex="0"><i class="fa fa-star"></i></a></li>
											<li><a href="#" tabindex="0"><i class="fa fa-star"></i></a></li>
										</ul>
										<span class="sale">FOR SALE</span>
									</div>
									<span class="price"><span class="add">$</span>32.000</span>
								</div>
							</div>
							<div class="box ">
								<div class="img-box">
									<div class="over">
										<div class="frame">
											<div class="block">
												<a href="portfolio-single-image.html" class="view" tabindex="0"><i class="fa fa-eye"></i></a>
												<a href="http://creative-wp.com/demos/fekra/images/real-estate/img05.jpg" class="expand lightbox" tabindex="0"><i class="fa fa-expand"></i></a>
												</div>
											</div>
										</div>
										<img src="http://creative-wp.com/demos/fekra/images/real-estate/img05.jpg" alt="image description">
									</div>
									<div class="text-box">
										<div class="txt">
										<h2>Akaarat Gadeda</h2>
										<p>Cairo <br>town house</p>
									</div>
									<div class="txt2">
										<ul class="star-list">
											<li class="active"><a href="#" tabindex="0"><i class="fa fa-star"></i></a></li>
											<li class="active"><a href="#" tabindex="0"><i class="fa fa-star"></i></a></li>
											<li class="active"><a href="#" tabindex="0"><i class="fa fa-star"></i></a></li>
											<li><a href="#" tabindex="0"><i class="fa fa-star"></i></a></li>
											<li><a href="#" tabindex="0"><i class="fa fa-star"></i></a></li>
										</ul>
										<span class="sale">FOR SALE</span>
									</div>
									<span class="price"><span class="add">$</span>32.000</span>
								</div>
							</div>
							<div class="box ">
								<div class="img-box">
									<div class="over">
										<div class="frame">
											<div class="block">
												<a href="portfolio-single-image.html" class="view" tabindex="0"><i class="fa fa-eye"></i></a>
												<a href="http://creative-wp.com/demos/fekra/images/real-estate/img05.jpg" class="expand lightbox" tabindex="0"><i class="fa fa-expand"></i></a>
												</div>
											</div>
										</div>
										<img src="http://creative-wp.com/demos/fekra/images/real-estate/img05.jpg" alt="image description">
									</div>
									<div class="text-box">
										<div class="txt">
										<h2>Akaarat Gadeda</h2>
										<p>Cairo <br>town house</p>
									</div>
									<div class="txt2">
										<ul class="star-list">
											<li class="active"><a href="#" tabindex="0"><i class="fa fa-star"></i></a></li>
											<li class="active"><a href="#" tabindex="0"><i class="fa fa-star"></i></a></li>
											<li class="active"><a href="#" tabindex="0"><i class="fa fa-star"></i></a></li>
											<li><a href="#" tabindex="0"><i class="fa fa-star"></i></a></li>
											<li><a href="#" tabindex="0"><i class="fa fa-star"></i></a></li>
										</ul>
										<span class="sale">FOR SALE</span>
									</div>
									<span class="price"><span class="add">$</span>32.000</span>
								</div>
							</div>
						</div>
					</div>
				</section>
				
			</main>
			
			
			
		<!-- footer of the page -->
			<footer id="footer" class="style14">
				
				<!-- footer cent -->
				<div id="contact" class="footer-cent bg-shark">
					<div class="container">
						<div class="row">
							<div class="col-xs-12 col-sm-6 col-md-3 info-box column1">
								<!-- f contact info2 -->
								<div class="f-contact-info2">
									<h5>Contact Information</h5>
									<address><i class="fa fa-map-marker"></i> <?php echo $location[0]->data;?> </address>
									<div class="mail-box2">
										<span class="mail-box"><i class="fa fa-envelope-o"></i> <a href="mailto:info@sooma.ae" class="email"> info@sooma.ae</a></span>
									</div>
									<div class="tel-holder">
										<span class="tel-box"><i class="fa fa-phone"></i> <a href="tel:00201008431112" class="tel">002- 01008431112</a></span>
									</div>
									<div class="mail-box2">
										<span class="mail-box"><i class="fa fa-link"></i> <a href="mailto:www.sooma.ae" class="email">sooma.ae</a></span>
									</div>
								</div>
							</div>
							<div class="col-xs-12 col-sm-6 col-md-3 column2">
								<!-- f video box -->
								<div class="f-video-box">
									<h5>Company Portfolio</h5>
									<div class="video-area">
										<div class="fluid-width-video-wrapper" style="padding-top: 56.2%;">
											<!--<iframe   src="<?php echo base_url();?>Assets/assets/DubaiFL.mp4" frameborder="0" webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen="" id="fitvid465872"></iframe>-->
										</div>

									</div>
								</div>
							</div>
							<div class="col-xs-12 col-sm-6 col-md-3 column3 clearfix-sm">
								<!-- f mailing form2 -->
								<div class="f-mailing-form2">
									<h5>Mailing List</h5>
									<div class="mailing-form2">
										<label>Please enter your email address for our mailing list to keep your self our lastest updated.</label>
										<div class="form-col">
											<!-- Begin MailChimp Signup Form -->
											<div id="mc_embed_signup">
												<form action="#" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate="">
													<div id="mc_embed_signup_scroll">
														<div class="mc-field-group">
															<label for="mce-EMAIL">Email Address </label>
															<input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" placeholder="Email Address">
														</div>
														<div id="mce-responses" class="clear">
															<div class="response" id="mce-error-response" style="display:none"></div>
															<div class="response" id="mce-success-response" style="display:none"></div>
														</div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
														<div style="position: absolute; left: -5000px;"><input type="text" name="b_cb2d5a07fdf0d86c96f260674_1103b14a3b" tabindex="-1" value=""></div>
														<div class="clear">
															<button type="submit" name="subscribe" id="mc-embedded-subscribe" class="btn button btn-submit"><i class="fa fa-paper-plane"></i></button>
														</div>
													</div>
												</form>
											</div>
											<!--End mc_embed_signup-->
										</div>
									</div>
								</div>
							</div>
							<div class="col-xs-12 col-sm-6 col-md-3 column4">
								<!-- f flicker -->
								<div class="f-flicker">
									
									<img src="<?php echo base_url();?>Assets/assets/LogoC.png"  alt="Sooma" style="width:70%; margin-top:-23%;"	>
									
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- footer bottom -->
				<div class="footer-bottom bg-dark-jungle">
					<div class="container">
						<div class="row">
							<div class="col-xs-12">
								<div class="bottom-box1">
									<!-- footer nav -->
									<ul class="list-inline footer-nav">
										<li><a href="#">Home</a></li>
										<li><a href="#">About Us</a></li>
										<li><a href="#">Careers</a></li>
										<li><a href="#">Privacy Policy</a></li>
										<li><a href="#">Use of terms</a></li>
									</ul>
									<span class="copyright">© 2016 Developed by <a href="#"> Overlimits LLC</a></span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</footer>
		</div>
		<div class="fa fa-chevron-up" id="gotoTop" style="display: none;"></div>
	</div>
	
	<!-- include jQuery library -->
	<div class="fit-vids-style" id="fit-vids-style" style="display: none;">­<style>.fluid-width-video-wrapper{width:100%;position:relative;padding:0;}.fluid-width-video-wrapper iframe,.fluid-width-video-wrapper object,.fluid-width-video-wrapper embed {position:absolute;top:0;left:0;width:100%;height:100%;}</style></div><script type="text/javascript" src="<?php echo base_url();?>Assets/assets/jquery-1.11.3.min.js"></script>
	<!-- include bootstrap JavaScript -->
	<script type="text/javascript" src="<?php echo base_url();?>Assets/assets/bootstrap.min.js"></script>
	<!-- include custom JavaScript -->
	<script type="text/javascript" src="<?php echo base_url();?>Assets/assets/jquery.main.js"></script>
	<!-- include plugins JavaScript -->
	<script type="text/javascript" src="<?php echo base_url();?>Assets/assets/plugins.js"></script>

<iframe id="font-resize-frame-71" class="font-resize-helper" style="position: absolute; width: 100em; height: 10px; top: -9999px; left: -9999px; border-width: 0px;"></iframe></body></html>