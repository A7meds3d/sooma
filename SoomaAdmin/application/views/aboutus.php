
			<!-- contain main informative part of the site -->
			<main id="main" role="main">
                <!-- page banner -->
				<header class="page-banner">
					<div class="stretch">
						<img alt="image description" src="<?php echo base_url();?>Assets/assets/Services2.jpg" style="width: 2139px; height: 445.625px; margin-top: -22.8125px; margin-left: 0px;">
					</div>
					<div class="container">
						<div class="row">
							<div class="col-xs-12">
								<div class="holder">
									<h1 class="heading text-capitalize">About us</h1>
									<p>Your challenge is our progress</p>
								</div>
								<ul class="breadcrumbs list-inline">
									<li><a href="#">HOME</a></li>
									<li class="active"><a href="#">About us</a></li>
								</ul>
							</div>
						</div>
					</div>
				</header>
				<!-- about section -->
				<section class="about-section padding-top-60 padding-bottom-60 container">
                    <!-- page heading -->
                    <header class="page-heading">
                        <h2 class="lime text-capitalize font-medium">In Few Words</h2>
                    </header>
					<div class="row">
						<div class="col-xs-12 col-md-6">
							<!-- box -->
							<div class="box fadeInUp animated" data-animate="fadeInUp" data-delay="400">
								<h3><i class="fa fa-caret-right"></i> Who are we</h3>
								<p><?php echo $who[0]->data;?></p>
							</div>
							<!-- box -->
							<div class="box fadeInUp animated" data-animate="fadeInUp" data-delay="400">
								<h3><i class="fa fa-caret-right"></i> Mission </h3>
								<p><?php echo $mission[0]->data;?></p>
							</div>
							<!-- box -->
							<div class="box fadeInUp animated" data-animate="fadeInUp" data-delay="400">
								<h3><i class="fa fa-caret-right"></i> Vision</h3>
								<p><?php echo $vision[0]->data;?></p>
							</div>
						</div>
						<div class="col-xs-12 col-sm-6">
							<div class="beans-slider fadeInRight animated" data-animate="fadeInRight" data-delay="200">
								<div class="beans-mask">
									<div class="beans-slideset slick-initialized slick-slider" role="toolbar">
										<!-- beans-slide -->
										<div aria-live="polite" class="slick-list draggable"><div class="slick-track" role="listbox" style="opacity: 1; width: 2850px; left: -1140px;"><div class="beans-slide slick-slide slick-cloned" data-slick-index="-1" aria-hidden="true" tabindex="-1" style="width: 570px;">
											<img src="http://creative-wp.com/demos/fekra/images/inner/img03.jpg" alt="image description" class="img-responsive">
										</div><div class="beans-slide slick-slide" data-slick-index="0" aria-hidden="true" tabindex="-1" role="option" aria-describedby="slick-slide00" style="width: 570px;">
											<img src="http://creative-wp.com/demos/fekra/images/inner/img03.jpg" alt="image description" class="img-responsive">
										</div><div class="beans-slide slick-slide slick-current slick-active" data-slick-index="1" aria-hidden="false" tabindex="-1" role="option" aria-describedby="slick-slide01" style="width: 570px;">
											<img src="./aboutus_files/img03.jpg" alt="image description" class="img-responsive">
										</div><div class="beans-slide slick-slide" data-slick-index="2" aria-hidden="true" tabindex="-1" role="option" aria-describedby="slick-slide02" style="width: 570px;">
											<img src="http://creative-wp.com/demos/fekra/images/inner/img03.jpg" alt="image description" class="img-responsive">
										</div><div class="beans-slide slick-slide slick-cloned" data-slick-index="3" aria-hidden="true" tabindex="-1" style="width: 570px;">
											<img src="http://creative-wp.com/demos/fekra/images/inner/img03.jpg" alt="image description" class="img-responsive">
										</div></div></div>
										<!-- beans-slide -->
										
										<!-- beans-slide -->
										
										
									<ul class="slick-dots" style="display: block;" role="tablist"><li class="" aria-hidden="true" role="presentation" aria-selected="true" aria-controls="navigation00" id="slick-slide00"><button type="button" data-role="none" role="button" aria-required="false" tabindex="0">1</button></li><li aria-hidden="false" role="presentation" aria-selected="false" aria-controls="navigation01" id="slick-slide01" class="slick-active"><button type="button" data-role="none" role="button" aria-required="false" tabindex="0">2</button></li><li aria-hidden="true" role="presentation" aria-selected="false" aria-controls="navigation02" id="slick-slide02" class=""><button type="button" data-role="none" role="button" aria-required="false" tabindex="0">3</button></li></ul></div>
								</div>
								
							</div>
						</div>
					</div>
				</section>
				<!-- container -->
				
				
				<!-- counter section -->
				<div class="counter-section">
					<div class="container">
						<div class="row">
							<div class="col-xs-12 col-sm-3">
								<div class="counter">
									<span class="num-counter num" data-from="10" data-to="56" data-refresh-interval="80" data-speed="3000" data-comma="true">56</span>
								</div>
								<span class="txt">Happy Clients </span>
							</div>
							<div class="col-xs-12 col-sm-3">
								<div class="counter">
									<span class="num-counter num" data-from="200" data-to="360" data-refresh-interval="80" data-speed="3000" data-comma="true">360</span>
								</div>
								<span class="txt">Properties</span>
							</div>
							<div class="col-xs-12 col-sm-3">
								<div class="counter">
									<span class="num-counter num" data-from="15" data-to="89" data-refresh-interval="80" data-speed="3000" data-comma="true">89</span>
								</div>
								<span class="txt">Project Finished</span>
							</div>
							<div class="col-xs-12 col-sm-3">
								<div class="counter">
									<span class="num-counter num" data-from="15" data-to="42" data-refresh-interval="80" data-speed="3000" data-comma="true">42</span>
								</div>
								<span class="txt">Employeer</span>
							</div>
						</div>
					</div>
					<div class="stretch">
						<img src="<?php echo base_url();?>Assets/assets/Bg1Overlay-17.png" alt="image description" style="width: 1899px; height: 850.594px; margin-top: -266.297px; margin-left: 0px;">
					</div>
				</div>

				<!-- container -->
				<section class="container padding-top-90 padding-bottom-90">
                    <!-- page heading -->
                    <header class="page-heading">
                        <h2 class="lime text-capitalize font-medium">Meet our team</h2>
                        <p>The Great team behind all this</p>
                    </header>
					<div class="row">
						<div class="col-xs-12 col-sm-6 fadeInUp animated" data-animate="fadeInUp" data-delay="400">
							<!-- team box -->
							<div class="team-box style2 right">
								<div class="img-box">
									<img src="<?php echo base_url();?>Assets/assets/Test.jpg" alt="image description" class="img-responsive">
									<span class="over black"></span>
									
								</div>
								<div class="block">
									<h3 class="heading">Omar Hafez</h3>
									<strong class="subtitle">Job Title</strong>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur pellentesque neque eget diam posuere porta. Quisque ut nulla at nunc vehicula lacinia.</p>
									<ul class="social-network list-inline">
										<li><a href="#"><i class="fa fa-facebook"></i></a></li>
										<li><a href="#"><i class="fa fa-twitter"></i></a></li>
										<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
										
									</ul>
								</div>
							</div>
						</div>
						<div class="col-xs-12 col-sm-6 fadeInUp animated" data-animate="fadeInUp" data-delay="400">
							<!-- team box -->
							<div class="team-box style2">
								<div class="img-box">
									<img src="<?php echo base_url();?>Assets/assets/Test.jpg" alt="image description" class="img-responsive">
									<span class="over black"></span>
									
								</div>
								<div class="block">
									<h3 class="heading">Hafez Fouad</h3>
									<strong class="subtitle">Job Title</strong>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur pellentesque neque eget diam posuere porta. Quisque ut nulla at nunc vehicula lacinia.</p>
									<ul class="social-network list-inline">
										<li><a href="#"><i class="fa fa-facebook"></i></a></li>
										<li><a href="#"><i class="fa fa-twitter"></i></a></li>
										<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
										
									</ul>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-sm-6 fadeInUp animated" data-animate="fadeInUp" data-delay="400">
							<!-- team box -->
							<div class="team-box style2 right">
								<div class="img-box">
									<img src="<?php echo base_url();?>Assets/assets/Test.jpg" alt="image description" class="img-responsive">
									<span class="over black"></span>
									
								</div>
								<div class="block">
									<h3 class="heading">Masood Al Zarooni</h3>
									<strong class="subtitle">Job Title</strong>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur pellentesque neque eget diam posuere porta. Quisque ut nulla at nunc vehicula lacinia.</p>
									<ul class="social-network list-inline">
										<li><a href="#"><i class="fa fa-facebook"></i></a></li>
										<li><a href="#"><i class="fa fa-twitter"></i></a></li>
										<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
										
									</ul>
								</div>
							</div>
						</div>
						<div class="col-xs-12 col-sm-6 fadeInUp animated" data-animate="fadeInUp" data-delay="400">
							<!-- team box -->
							<div class="team-box style2">
								<div class="img-box">
									<img src="<?php echo base_url();?>Assets/assets/Test.jpg" alt="image description" class="img-responsive">
									<span class="over black"></span>
									
								</div>
								<div class="block">
									<h3 class="heading">Sameh Shaaban</h3>
									<strong class="subtitle">Job Title</strong>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur pellentesque neque eget diam posuere porta. Quisque ut nulla at nunc vehicula lacinia.</p>
									<ul class="social-network list-inline">
										<li><a href="#"><i class="fa fa-facebook"></i></a></li>
										<li><a href="#"><i class="fa fa-twitter"></i></a></li>
										<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				
				<!--	<div class="row text-center padding-top-30 ">
					  <a href="http://creative-wp.com/demos/fekra/about-us.html#" class="btn btn-dark">VIEW ALL MEMBERS</a>
					</div> -->
				</section>
				
				

				<!-- testimon section -->
				<div class="testimon-section">
					<div class="container">
						<div class="row">
							<div class="col-xs-12">
								<span class="icon"><i class="fa fa-quote-left"></i></span>
								<div class="beans-slider" data-rotate="true">
									<div class="beans-mask">
										<div class="beans-slideset slick-initialized slick-slider" role="toolbar">
											<!-- beans-slide -->
											<div aria-live="polite" class="slick-list draggable"><div class="slick-track" role="listbox" style="opacity: 1; width: 5850px; left: -2340px;"><div class="beans-slide slick-slide slick-cloned" data-slick-index="-1" aria-hidden="true" tabindex="-1" style="width: 1170px;">
												<blockquote>
													<q><?php echo $ceo[0]->data;?></q>
													<img src="<?php echo base_url();?>Assets/assets/Test.jpg"  style="width:10%;"alt="image description" class="img">
													<cite>
														<span class="name">Omar Hafez</span>
														<span class="comp-name"><a href="#" tabindex="-1">SOOMA - CEO</a></span>
													</cite>
												</blockquote>
											</div></div></div>
											<!-- beans-slide -->
											
											<!-- beans-slide -->
											
									<!--	<ul class="slick-dots" style="display: block;" role="tablist"><li class="" aria-hidden="true" role="presentation" aria-selected="true" aria-controls="navigation10" id="slick-slide10"><button type="button" data-role="none" role="button" aria-required="false" tabindex="0">1</button></li><li aria-hidden="false" role="presentation" aria-selected="false" aria-controls="navigation11" id="slick-slide11" class="slick-active"><button type="button" data-role="none" role="button" aria-required="false" tabindex="0">2</button></li><li aria-hidden="true" role="presentation" aria-selected="false" aria-controls="navigation12" id="slick-slide12" class=""><button type="button" data-role="none" role="button" aria-required="false" tabindex="0">3</button></li></ul>--></div>
									</div>
									
								</div>
							</div>
						</div>
					</div>
					<div class="parallax-holder">
						<div class="parallax-frame" style="padding-bottom: 360px; background-image: url('<?php echo base_url();?>Assets/assets/Bg1Overlay-17.png'); background-attachment: fixed; background-size: 1899px 1424.25px; background-position: 50% -252.625px; background-repeat: no-repeat;">
							<img src="<?php echo base_url();?>Assets/assets/Bg1Overlay-17.png" height="1440" width="1920" alt="image description" style="visibility: hidden;"></div>
					</div>
				</div>


			</main>
			

