
			<!-- contain main informative part of the site -->
			<main id="main" role="main">
                <!-- page banner -->
				<header class="page-banner">
					<div class="stretch">
						<img alt="image description" src="<?php echo base_url();?>Assets/assets/Services.png" style="width: 2139px; height: 445.625px; margin-top: -22.8125px; margin-left: 0px;">
					</div>
					<div class="container">
						<div class="row">
							<div class="col-xs-12">
								<div class="holder">
									<h1 class="heading text-capitalize">Projects</h1>
									<p>Your challenge is our progress</p>
								</div>
								<ul class="breadcrumbs list-inline">
									<li><a href="#">HOME</a></li>
									<li class="active"><a href="#">Projects</a></li>
								</ul>
							</div>
						</div>
					</div>
				</header>
				<!-- wedo section -->
				<!-- port single -->

				<?php
				if(!isset($properties))echo 'no projects yet';
				foreach ($properties as $project) {
					echo '	<section class="port-single padding-top-100">
					<div class="container">
						<div class="row">
							<div class="col-xs-12 text-center">
								<a href="" class="prev"><i class="fa fa-angle-left"></i></a>
								<a href="" class="dashboard"><i class="fa fa-th"></i></a>
								<a href="" class="next"><i class="fa fa-angle-right"></i></a>
							</div>
						</div>

						<div class="row text-upercase">
							<div class="col-xs-12 text-center">
								<h2>'.$project->name.'</h2>
								<p>IN TWINS CITY</p>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 video-area text-center">
								<img  width="20%" height="20%" src="'.base_url().$project->link.'" />
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-sm-6 col-md-8">
								<h3>About the project</h3>
								<p>'.$project->about.' </p>
								
							</div>
							<div class="col-xs-12 col-sm-6 col-md-4">
								<h3>PROJECT INFO</h3>
								<ul>
									<li>
										<i class="fa fa-user"></i> <strong>Project Name:</strong> '.$project->name.'
									</li>
								
									<li>
										<i class="fa fa-tags"></i> <strong>Bedrooms:</strong> '.$project->bedrooms.'

									</li>
								</ul>
								
							</div>
						</div>
					</div>
				</section>';
				}
				?>
			
				<!-- port aside -->
				<!-- <aside class="port-aside bg-grey dark-bottom-border padding-top-100 padding-bottom-90">
                   <div class="container">
                       <div class="row">
                           <div class=""> -->
                                <!-- page heading small -->
                              <!--   <header class="page-heading left-align col-xs-12 col-md-12">
                                    <h2 class="lime text-capitalize font-light">Similar Projects</h2>
                                </header>
                            </div>
                       </div>
                    </div> -->
					<!-- beans-stepslider -->
					<!-- <div class="beans-stepslider work-slider">
						<div class="beans-mask margin-bottom-60">
							<div class="beans-slideset slick-initialized slick-slider" role="toolbar">
							
								
							<div aria-live="polite" class="slick-list draggable">
								<div class="slick-track" role="listbox" style="opacity: 1; width: 1200px; left: -2850px;">
								<div class="beans-slide slick-slide slick-cloned" tabindex="-1" role="option" aria-describedby="slick-slide02" style="width: 475px;" data-slick-index="-4" aria-hidden="true"> -->
									<!-- portfolio block nospace -->
									<!-- <div class="portfolio-block nospace"> -->
										<!-- box -->
										<!-- <div class="box">
											<div class="over">
												<div class="holder">
													<div class="frame">
														<div class="over-frame">
															<strong class="title upper">NEW BAGS FASHION</strong>
															<p>FASHION</p>
														</div>
													</div>
												</div>
												<a href="http://creative-wp.com/demos/fekra/images/img13.jpg" class="search lightbox" tabindex="-1"><i class="fa fa-search"></i></a>
												<a href="#" class="link" tabindex="-1"><i class="fa fa-link"></i></a>
											</div>
											<img src="http://creative-wp.com/demos/fekra/images/img13.jpg" alt="image description">
										</div>
									</div>
								</div><div class="beans-slide slick-slide slick-cloned" tabindex="-1" role="option" aria-describedby="slick-slide03" style="width: 475px;" data-slick-index="-3" aria-hidden="true"> -->
									<!-- portfolio block nospace -->
									<!-- <div class="portfolio-block nospace"> -->
										<!-- box -->
										<!-- <div class="box">
											<div class="over">
												<div class="holder">
													<div class="frame">
														<div class="over-frame">
															<strong class="title upper">SOME AWOSOME WORK</strong>
															<p>ILLUSTRATION</p>
														</div>
													</div>
												</div>
												<a href="http://creative-wp.com/demos/fekra/images/img13.jpg" class="search lightbox" tabindex="-1"><i class="fa fa-search"></i></a>
												<a href="" class="link" tabindex="-1"><i class="fa fa-link"></i></a>
											</div>
											<img src="http://creative-wp.com/demos/fekra/images/img13.jpg" alt="image description">
										</div>
									</div>
								</div><div class="beans-slide slick-slide slick-cloned" tabindex="-1" role="option" aria-describedby="slick-slide04" style="width: 475px;" data-slick-index="-2" aria-hidden="true"> -->
									<!-- portfolio block nospace -->
									<!-- <div class="portfolio-block nospace"> -->
										<!-- box -->
										<!-- <div class="box">
											<div class="over">
												<div class="holder">
													<div class="frame">
														<div class="over-frame">
															<strong class="title upper">OUR PRODUCTS</strong>
															<p>PRODUCTS</p>
														</div>
													</div>
												</div>
												<a href="http://creative-wp.com/demos/fekra/images/img13.jpg" class="search lightbox" tabindex="-1"><i class="fa fa-search"></i></a>
												<a href="" class="link" tabindex="-1"><i class="fa fa-link"></i></a>
											</div>
											<img src="http://creative-wp.com/demos/fekra/images/img13.jpg" alt="image description">
										</div>
									</div>
								</div>
				</aside>-->


			</main>
			
	