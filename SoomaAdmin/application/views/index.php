<!DOCTYPE html>

<html class=""><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<!-- set the encoding of your site -->
	<meta charset="utf-8">
	<!-- set the viewport width and initial-scale on mobile devices -->
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- set the title of your site -->
	<title>SOOMA Real Estate</title>
	<!-- ========= Favicon Icons ========= -->
	<link rel="shortcut icon" href="#">
	<!-- Standard iPhone Touch Icon-->
	<link rel="apple-touch-icon" sizes="57x57" href="#">
	<!-- Retina iPhone Touch Icon-->
	<link rel="apple-touch-icon" sizes="114x114" href="#">
	<!-- Standard iPad Touch Icon-->
	<link rel="apple-touch-icon" sizes="72x72" href="#">
	<!-- Retina iPad Touch Icon-->
	<link rel="apple-touch-icon" sizes="144x144" href="#">
	<!-- include Google fonts  -->
	<link href="<?php echo base_url();?>Assets/assets/css" rel="stylesheet" type="text/css">
	<!-- include the site stylesheet of bootstrap  -->
	<link rel="stylesheet" href="<?php echo base_url();?>Assets/assets/bootstrap.css">
	<!-- include the font awesome stylesheet  -->
	<link rel="stylesheet" href="<?php echo base_url();?>Assets/assets/font-awesome/font-awesome/css/font-awesome.min.css">
	<!-- include the stylesheets of headers and footer of the page  -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>Assets/assets/page-assets.css">
	<!-- include the helping elements stylesheets of  the page  -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>Assets/assets/helper-elements.css">
	<!-- include the site stylesheet  -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>Assets/assets/style.css">
	<!-- include the site color stylesheet  -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>Assets/assets/di-serria.css">
    <!-- include the site animation stylesheet  -->
    <link rel="stylesheet" href="<?php echo base_url();?>Assets/assets/animate.css" type="text/css">
  
<style>
	

.Selects
	{
	border: 0;
	float: left;
	width: 23.2%;
	height: 42px;
	outline: none;
	color: #848484;
	background: #fff;
	margin: 0 0 31px 2.4%;
	padding: 10px 20px 10px 20px;
	font: 16px/20px "Lato", Georgia, "Times New Roman", Times, serif;
}


</style>
<style type="text/css">.js-tab-hidden{position:absolute !important;left:-9999px !important;top:-9999px !important;display:block !important}</style>
<style type="text/css">.fancybox-margin{margin-right:21px;}</style><link rel="stylesheet" type="text/css" href="<?php echo base_url();?>Assets/assets/di-serria.css"></head>
<body class="">
	<!-- Page pre loader -->
    <div id="pre-loader" style="">
        <div class="loader-holder">
            <div class="frame">
                <img src="<?php echo base_url();?>Assets/assets/Logo1B.png" style="width:200px;text-align:center; " alt="Sooma">
                <div class="spinner7" style="width: 170px;">
                    <div class="circ1"></div>
                    <div class="circ2"></div>
                    <div class="circ3"></div>
                    <div class="circ4"></div>
                </div>
            </div>
        </div>
    </div>
	<!-- main container of all the page elements -->
	<div id="wrapper">
		<div class="w1" >
			<!-- header of the page -->
			<header id="header" class="style14" >
				<div class="container-fluid">
					<div class="row" id="Hide">
						<div class="col-xs-12">
							<!-- page logo -->
							
							<!-- main navigation of the page -->
							<nav id="nav">

								<a href="#" class="logo">
									<img src="<?php echo base_url();?>Assets/assets/Logo2W.png"  alt="Sooma" style="height: 50px;width: auto;    margin-top: -25%;" class="img-responsive w-logo">
									<img src="<?php echo base_url();?>Assets/assets/Logo2B.png" alt="Sooma" style="height: 50px;width: auto;    margin-top: -25%;" class="img-responsive b-logo">
								</a>
							
								<a href="#" class="nav-opener"><i class="fa fa-bars"></i></a>
								<div class="nav-holder">
									<img src="<?php echo base_url();?>Assets/assets/Logo2W.png" id="SoomaLogoMob" alt="Sooma" style="width:50%;margin:10px auto 0 auto;   " class="img-responsive b-logo">
									<ul class="list-inline nav-top">
										<li class="active">
											<a href="<?php echo base_url();?>index.php//Home">Home</a>
										</li>
										
										<!-- <li class="">
											<a href="<?php echo base_url();?>index.php/Home/Aboutus">About us</a>
										</li> -->
										
										<li class="has-drop">
											<a href="<?php echo base_url();?>index.php/Home/Project"s>Projects</a>
											<div class="drop">
												<ul class="list-unstyled">
													<li class="">
														<a href="<?php echo base_url();?>index.php/Home/Project">Al Fouad</a>
													</li>
													
													
												</ul>
											</div>
										</li>
										<li class="">
												<a href="<?php echo base_url();?>index.php/Properties/Gallery">Live Gallery</a>
											</li>
											<li class="">
												<a href="<?php echo base_url();?>index.php/Home/CS">Property Listings</a>
											</li>
										
										<li class="">
											<a href="<?php echo base_url();?>index.php/Home/Contact">Contact us</a>
										</li>
									</ul>
								</div>
							</nav>
							<!-- icon list -->
							<ul class="list-unstyled icon-list">
								<li>
									<a href="#" class="search-opener opener-icons"><i class="fa fa-search"></i></a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</header>
			<!-- search popup -->
			<div class="search-popup win-height">
				<div class="holder">
					<div class="container">
						<div class="row">
							<div class="col-xs-12">
								<a href="#" class="close-btn">close</a>
								<form action="#" class="search-form">
									<fieldset>
										<input type="search" placeholder="search..." class="search">
										<button class="submit"><i class="fa fa-search"></i></button>
									</fieldset>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- contain main informative part of the site -->
			<main id="main">
				<!-- architecture gallery -->

				
				<section id="home" class="beans-slider architecture-gallery gallery-js-ready">

					<div class="navright" id="navmenu">
						<div class="Navigationbutts" id="NavigationHome"><a href="<?php echo base_url();?>index.php">
							<span> <img class="Navigationimage" src="<?php echo base_url();?>Assets/assets/icon-13.png"> </span>
							<span class="NavigationContent"> Home </span></a>

						</div>
					
						<div class="Navigationbutts" id="NavigationAbout"><a href="<?php echo base_url();?>index.php/Home/Listing">
							<span> <img class="Navigationimage" src="<?php echo base_url();?>Assets/assets/icon-12.png"> </span>
							<span class="NavigationContent"> Property listing </span></a>

						</div>

						<div class="Navigationbutts" id="NavigationProp"><a href="<?php echo base_url();?>index.php/Properties/gallery">
							<span> <img class="Navigationimage" src="<?php echo base_url();?>Assets/assets/icon-14.png"> </span>
							<span class="NavigationContent"> Live Gallery </span></a>

						</div>

						<div class="Navigationbutts" id="NavigationContact"><a href="<?php echo base_url();?>index.php/Home/Contact">
							<span> <img class="Navigationimage" src="<?php echo base_url();?>Assets/assets/icon-16.png"> </span>
							<span class="NavigationContent"> Contact Us</span></a>

						</div>
						<!--
						<div class="Navigationbutts" id="NavigationCareer"><a href="<?php echo base_url();?>index.php/Home/Aboutus">
							<span> <img class="Navigationimage" src="<?php echo base_url();?>Assets/assets/icon-15.png"> </span>
							<span class="NavigationContent"> About Us </span></a>

						</div>
						-->
						
					</div>

					<div class="beans-mask" style="height: 919px;">
						<div class="beans-slideset" style="position: relative; height: 100%; margin-left: -1899px;">
							<!-- beans-slide -->
							<div class="beans-slide" style="position: absolute; left: 3798px; width: 1899px;">
								<div class="stretch">
									<!-- <img src="<?php echo base_url();?>Assets/assets/Bg1Overlay-17.png" alt="image description" style="width: 1899px; height: 1068.19px; margin-top: -74.5938px; margin-left: 0px;">
								-->

									<video class="vidbg" autoplay loop poster="<?php echo base_url();?>Assets/assets/DubaiFL.jpg" id="bgvid">
									    
									    <source src="<?php echo base_url();?>Assets/assets/vid1.mp4" type="video/mp4"/>
									</video>
								</div>
								<div class="container">
									<div class="row">
										<div class="col-xs-12">
											<div class="win-height holder">
												<div class="frame">
													<span class="title"><img src="<?php echo base_url();?>Assets/assets/LogoC.png"  alt="Sooma" style="width:33%;"	></span>
													</br>
													<p>Your Home.</p>
													<a href="#searchsection" class="btn btn-history">Search Now for your Future Home</a>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							
							<div class="beans-slide" style="position: absolute; left: 3798px; width: 1899px;">
								<div class="stretch">
									<!-- <img src="<?php echo base_url();?>Assets/assets/Bg1Overlay-17.png" alt="image description" style="width: 1899px; height: 1068.19px; margin-top: -74.5938px; margin-left: 0px;">
								-->

									<video autoplay loop poster="<?php echo base_url();?>Assets/assets/DubaiFL.jpg" id="bgvid">
									    
									    <source src="<?php echo base_url();?>Assets/assets/DubaiFL.mp4" type="video/mp4"/>
									</video>
								</div>
								<div class="container">
									<div class="row">
										<div class="col-xs-12">
											<div class="win-height holder">
												<div class="frame">
													<span class="title"><img src="<?php echo base_url();?>Assets/assets/LogoC.png"  alt="Sooma" style="width:50%;"	></span>
													</br>
													<p>Listen better. Plan better. Build better</p>
													<form action="#" class="signup-form">
									<fieldset>
										<div class="frame"  style="    margin-bottom: -10px;">
											<select class="Selects" name="type">
												<option value="null">Any Emirate</option>
		                                        <option value="0">Dubai</option>
		                                        <option value="1">Sharqa</option>
		                                        <option value="2">El Ain</option>
		                                    </select>
											<select  class="Selects" name="type">
												<option value="null">Any Location</option>
		                                        <option value="0">Dubai</option>
		                                        <option value="1">Sharqa</option>
		                                        <option value="2">El Ain</option>
		                                    </select>
		                                    <select class="Selects" name="type">
												<option value="null">Any Type</option>
		                                        <option value="0">Rental</option>
		                                        <option value="1">Sale</option>
		                                        
		                                    </select>
											<select  class="Selects" name="type">
												<option value="null">Any Sub Location</option>
		                                        <option value="0">Dubai</option>
		                                        <option value="1">Sharqa</option>
		                                        <option value="2">El Ain</option>
		                                    </select>
											
										</div>
										<div class="frame">
											<select class="Selects" name="type">
												<option value="null">Min Price</option>
		                                        <option value="0">Dubai</option>
		                                        <option value="1">Sharqa</option>
		                                        <option value="2">El Ain</option>
		                                    </select>
											<select  class="Selects" name="type">
												<option value="null">Max Price</option>
		                                        <option value="0">Dubai</option>
		                                        <option value="1">Sharqa</option>
		                                        <option value="2">El Ain</option>
		                                    </select>
		                                    <select class="Selects" name="type">
												<option value="null">No Bedrooms</option>
		                                        <option value="0">Rental</option>
		                                        <option value="1">Sale</option>
		                                        
		                                    </select>
											<select  class="Selects" name="type">
												<option value="null">No Bathrooms</option>
		                                        <option value="0">Dubai</option>
		                                        <option value="1">Sharqa</option>
		                                        <option value="2">El Ain</option>
		                                    </select>
											
										</div>
										<button class="btn btn-submit">SEARCH For your Future Home</button>
									</fieldset>
								</form>																			
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							
						</div>
					</div>
					
				</section>
				


				<section id="services" class="archi-columns" style="margin-top:50px">
					<div class="container">
						<div class="row">
							<div class="col-xs-12">
								<div class="coll fadeInUp animated" data-animate="fadeInUp" data-delay="100">
									<img src="<?php echo base_url();?>Assets/assets/img01(1).jpg" alt="image description" class="img-responsive">
									<div class="block">
										<h2><a href="#">AL FOUAD</a></h2>
										<p>Our team of professional architects will make sure to fill your requests with the most creative designs and you can ever imagine </p>
										<a href="#" class="more"><i class="fa fa-angle-double-right"></i>
										</a>
									</div>
								</div>
								<div class="coll fadeInUp animated" data-animate="fadeInUp" data-delay="200">
									<img src="<?php echo base_url();?>Assets/assets/img02(1).jpg" alt="image description" class="img-responsive">
									<div class="block">
										<h2><a href="#">ENINGINER HAFEZ LIVING LEGENDS VILLA</a></h2>
										<p>Interior decoration trends are changing rapidly, which is why we are always up to date and know what is trending in the interiors market. </p>
										<a href="#" class="more"><i class="fa fa-angle-double-right"></i></a>
									</div>
								</div>
								<div class="coll fadeInUp animated" data-animate="fadeInUp" data-delay="300">
									<img src="<?php echo base_url();?>Assets/assets/img03.jpg" alt="image description" class="img-responsive">
									<div class="block">
										<h2><a href="#">ENINGINER HAFEZ LIVING LEGENDS LAND</a></h2>
										<p>There is much you can do with a landscape. At SOOMA, we love creating remarkable experiences out of nothing! This is why we mastered the art of landscape design and construction.   </p>
										<a href="#" class="more"><i class="fa fa-angle-double-right"></i></a>
									</div>
								</div>
							</div>
						</div>
					</div>

				</section>

				<!-- archi columns -->
				<section id="searchsection" class="app-mainbanner">
					<div class="container">
						<div class="row">
							<div class="col-xs-12 holder">
								<h1>Search Now<br>for your <span class="add">Future Home</span></h1>
								
								<form action="#" class="signup-form">
									<fieldset>
										<div class="frame"  style="    margin-bottom: -10px;">
											<select class="Selects" name="type">
												<option value="null">Any Emirate</option>
		                                        <option value="0">Dubai</option>
		                                        <option value="1">Sharqa</option>
		                                        <option value="2">El Ain</option>
		                                    </select>
											<select  class="Selects" name="type">
												<option value="null">Any Location</option>
		                                        <option value="0">Dubai</option>
		                                        <option value="1">Sharqa</option>
		                                        <option value="2">El Ain</option>
		                                    </select>
		                                    <select class="Selects" name="type">
												<option value="null">Any Type</option>
		                                        <option value="0">Rental</option>
		                                        <option value="1">Sale</option>
		                                        
		                                    </select>
											<select  class="Selects" name="type">
												<option value="null">Any Sub Location</option>
		                                        <option value="0">Dubai</option>
		                                        <option value="1">Sharqa</option>
		                                        <option value="2">El Ain</option>
		                                    </select>
											
										</div>
										<div class="frame">
											<select class="Selects" name="type">
												<option value="null">Min Price</option>
		                                        <option value="0">Dubai</option>
		                                        <option value="1">Sharqa</option>
		                                        <option value="2">El Ain</option>
		                                    </select>
											<select  class="Selects" name="type">
												<option value="null">Max Price</option>
		                                        <option value="0">Dubai</option>
		                                        <option value="1">Sharqa</option>
		                                        <option value="2">El Ain</option>
		                                    </select>
		                                    <select class="Selects" name="type">
												<option value="null">No Bedrooms</option>
		                                        <option value="0">Rental</option>
		                                        <option value="1">Sale</option>
		                                        
		                                    </select>
											<select  class="Selects" name="type">
												<option value="null">No Bathrooms</option>
		                                        <option value="0">Dubai</option>
		                                        <option value="1">Sharqa</option>
		                                        <option value="2">El Ain</option>
		                                    </select>
											
										</div>
										<button class="btn btn-submit">SEARCH For your Future Home</button>
									</fieldset>
								</form>
							</div>
						</div>
					</div>
					<div class="stretch">
						<img alt="image description" src="<?php echo base_url();?>Assets/assets/Services2.jpg" data-animate="fadeInUpRight" data-delay="300" class="fadeInUpRight animated" style="width: 1899px; height: 1186.88px; margin-top: -243.438px; margin-left: 0px;">
					</div>
				</section>

				<!-- archi projects 
				<section id="projects" class="archi-projects">
					<div class="stretch">
						<img src="<?php echo base_url();?>Assets/assets/shutterstock_132104546(2).jpg" alt="image description" style="width: 1899px; height: 1266px; margin-top: -165.5px; margin-left: 0px;">
					</div>
					<div class="container">
                        <!-- page heading small 
                        <header class="page-heading small">
                            <div class="heading">
                                <h2 class="lime text-capitalize font-medium margin-bottom-10">Our Projects</h2>
                                <p>Our projects are spreading around UAE with a clear vision of becoming a leading role player in this growing market. </p>
                            </div>
                        </header>
						<div class="row">
							<div class="col-xs-12">
								<!-- beans-slider 
								<div class="beans-slider gallery-js-ready">
									<div class="beans-mask" style="height: 560px;">
										<div class="beans-slideset" style="position: relative; height: 100%; margin-left: -1170px;">
											<!-- beans-slide 1
                                                                                        <?php foreach($props as $prop) {?>
                                                                                            <div class="beans-slide active" style="position: absolute; left: 1170px; width: 1170px;">
                                                                                                    
                                                                                                    <div class="text">
                                                                                                    	<div class="img-box">
                                                                                                            <img  src="<?php echo base_url().$prop->link;?>" alt="image description" class="img-responsive">
                                                                                                    </div>

                                                                                                            <h3><span class="add"><?php echo $prop->name?></span></h3>
                                                                                                            <span class="title">RESTAURANT INTERIOR.</span>
                                                                                                            <span class="subtitle">Rome - Italy</span>
                                                                                                            <p><?php echo $prop->about;?></p></br></br></br></br>
                                                                                                            <a class="btn btn-history" href="<?php echo base_url();?>index.php/Home/Project">Details</a>
                                                                                                    </div>
                                                                                            </div>
                                                                                        <?php } ?>
										</div>
									</div>
									<div class="beans-pagination hidden-xs"><ul><li class="active"><a href="#">1</a></li><li><a href="#">2</a></li><li><a href="#">3</a></li>
										<li><a href="#">4</a></li><li><a href="#">5</a></li></ul></div>
								</div>
							</div>
						</div>
					</div>
				</section>
				-->
				<section class="realestate-services bg-grey">
					<div class="container">
                        <!-- page heading -->
                        <header class="page-heading color-stack style2">
                            <div class="heading">
                                <h2 class="lime text-capitalize font-medium">NEWEST PROPERTIES<span class="dot">.</span></h2>
                                <p>Find all what you dream to find newest properties</p>
                            </div>
                        </header>
						<div class="row">
							<div class="col-xs-12">
                                <!-- isotop controls4 -->
                                <nav class="isotop-controls4 no-bg margin-bottom-30">
                                    <ul class="list-inline tabset margin-bottom-10">
                                        <li class="active"><a href="#tab1-2">PROPERTIES</a></li>
                                        <li><a href="#tab2-2">LANDS</a></li>
                                        <li><a href="#tab3-2">OFFICES</a></li>
                                    </ul>
                                </nav>
								<div class="tab-content">
									<div id="tab1-2" class="" style="display: block;">
										<!-- beans slider -->
										<div class="beans-slider">
											
											
											<div class="beans-mask">
												<div class="beans-slideset slick-initialized slick-slider"><button type="button" data-role="none" class="slick-prev slick-arrow" aria-label="Previous" role="button" style="display: block;">Previous</button>
													<!-- beans slide -->
													<div aria-live="polite" class="slick-list draggable"><div class="slick-track" role="listbox" style="opacity: 1; width: 4680px; left: -2340px;"><div class="beans-slide slick-slide slick-cloned" data-slick-index="-1" aria-hidden="true" tabindex="-1" style="width: 1170px;">
														<!-- box -->
														<div class="box">
															<div class="img-box">
																<div class="over">
																	<div class="frame">
																		<div class="block">
																			<a href="portfolio-single-image.html" class="view" tabindex="-1"><i class="fa fa-eye"></i></a>
																			<a href="http://creative-wp.com/demos/fekra/images/real-estate/img04.jpg" class="expand lightbox" tabindex="-1"><i class="fa fa-expand"></i></a>
																		</div>
																	</div>
																</div>
																<img src="http://creative-wp.com/demos/fekra/images/real-estate/img02.jpg" alt="image description">
															</div>
															<div class="text-box">
																<div class="txt">
																	<h2>Akaarat Gadeda</h2>
																	<p>Cairo <br>town house</p>
																</div>
																<div class="txt2">
																	<ul class="star-list">
																		<li class="active"><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li class="active"><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li class="active"><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																	</ul>
																	<span class="sale">FOR SALE</span>
																</div>
																<span class="price"><span class="add">$</span>56.000</span>
															</div>
														</div>
														<!-- box -->
														<div class="box">
															<div class="img-box">
																<div class="over">
																	<div class="frame">
																		<div class="block">
																			<a href="portfolio-single-image.html" class="view" tabindex="-1"><i class="fa fa-eye"></i></a>
																			<a href="http://creative-wp.com/demos/fekra/images/real-estate/img02.jpg" class="expand lightbox" tabindex="-1"><i class="fa fa-expand"></i></a>
																		</div>
																	</div>
																</div>
																<img src="http://creative-wp.com/demos/fekra/images/real-estate/img03.jpg" alt="image description">
															</div>
															<div class="text-box">
																<div class="txt">
																	<h2>Akaarat Gadeda</h2>
																	<p>Cairo <br>town house</p>
																</div>
																<div class="txt2">
																	<ul class="star-list">
																		<li class="active"><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li class="active"><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li class="active"><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																	</ul>
																	<span class="sale">FOR SALE</span>
																</div>
																<span class="price"><span class="add">$</span>32.000</span>
															</div>
														</div>
														<!-- box -->
														<div class="box">
															<div class="img-box">
																<div class="over">
																	<div class="frame">
																		<div class="block">
																			<a href="portfolio-single-image.html" class="view" tabindex="-1"><i class="fa fa-eye"></i></a>
																			<a href="http://creative-wp.com/demos/fekra/images/real-estate/img03.jpg" class="expand lightbox" tabindex="-1"><i class="fa fa-expand"></i></a>
																		</div>
																	</div>
																</div>
																<img src="http://creative-wp.com/demos/fekra/images/real-estate/img04.jpg" alt="image description">
															</div>
															<div class="text-box">
																<div class="txt">
																	<h2>TOGMAA COMPUND</h2>
																	<p>Cairo <br>town house</p>
																</div>
																<div class="txt2">
																	<ul class="star-list">
																		<li class="active"><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li class="active"><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li class="active"><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																	</ul>
																	<span class="sale">FOR SALE</span>
																</div>
																<span class="price"><span class="add">$</span>226.000</span>
															</div>
														</div>
														<!-- box -->
														<div class="box">
															<div class="img-box">
																<div class="over">
																	<div class="frame">
																		<div class="block">
																			<a href="portfolio-single-image.html" class="view" tabindex="-1"><i class="fa fa-eye"></i></a>
																			<a href="http://creative-wp.com/demos/fekra/images/real-estate/img05.jpg" class="expand lightbox" tabindex="-1"><i class="fa fa-expand"></i></a>
																		</div>
																	</div>
																</div>
																<img src="http://creative-wp.com/demos/fekra/images/real-estate/img05.jpg" alt="image description">
															</div>
															<div class="text-box">
																<div class="txt">
																	<h2>HILLS PLAZA</h2>
																	<p>Cairo <br>town house</p>
																</div>
																<div class="txt2">
																	<ul class="star-list">
																		<li class="active"><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li class="active"><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li class="active"><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																	</ul>
																	<span class="sale">FOR SALE</span>
																</div>
																<span class="price"><span class="add">$</span>158.000</span>
															</div>
														</div>
													</div><div class="beans-slide slick-slide" data-slick-index="0" aria-hidden="true" tabindex="-1" role="option" aria-describedby="slick-slide10" style="width: 1170px;">
														<!-- box -->
														<div class="box">
															<div class="img-box">
																<div class="over">
																	<div class="frame">
																		<div class="block">
																			<a href="portfolio-single-image.html" class="view" tabindex="-1"><i class="fa fa-eye"></i></a>
																			<a href="http://creative-wp.com/demos/fekra/images/real-estate/img02.jpg" class="expand lightbox" tabindex="-1"><i class="fa fa-expand"></i></a>
																		</div>
																	</div>
																</div>
																<img src="http://creative-wp.com/demos/fekra/images/real-estate/img02.jpg" alt="image description">
															</div>
															<div class="text-box">
																<div class="txt">
																	<h2>Akaarat Gadeda</h2>
																	<p>Cairo <br>town house</p>
																</div>
																<div class="txt2">
																	<ul class="star-list">
																		<li class="active"><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li class="active"><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li class="active"><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																	</ul>
																	<span class="sale">FOR SALE</span>
																</div>
																<span class="price"><span class="add">$</span>56.000</span>
															</div>
														</div>
														<!-- box -->
														<div class="box">
															<div class="img-box">
																<div class="over">
																	<div class="frame">
																		<div class="block">
																			<a href="portfolio-single-image.html" class="view" tabindex="-1"><i class="fa fa-eye"></i></a>
																			<a href="http://creative-wp.com/demos/fekra/images/real-estate/img03.jpg" class="expand lightbox" tabindex="-1"><i class="fa fa-expand"></i></a>
																		</div>
																	</div>
																</div>
																<img src="http://creative-wp.com/demos/fekra/images/real-estate/img03.jpg" alt="image description">
															</div>
															<div class="text-box">
																<div class="txt">
																	<h2>Akaarat Gadeda</h2>
																	<p>Cairo <br>town house</p>
																</div>
																<div class="txt2">
																	<ul class="star-list">
																		<li class="active"><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li class="active"><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li class="active"><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																	</ul>
																	<span class="sale">FOR SALE</span>
																</div>
																<span class="price"><span class="add">$</span>32.000</span>
															</div>
														</div>
														<!-- box -->
														<div class="box">
															<div class="img-box">
																<div class="over">
																	<div class="frame">
																		<div class="block">
																			<a href="portfolio-single-image.html" class="view" tabindex="-1"><i class="fa fa-eye"></i></a>
																			<a href="http://creative-wp.com/demos/fekra/images/real-estate/img04.jpg" class="expand lightbox" tabindex="-1"><i class="fa fa-expand"></i></a>
																		</div>
																	</div>
																</div>
																<img src="http://creative-wp.com/demos/fekra/images/real-estate/img04.jpg" alt="image description">
															</div>
															<div class="text-box">
																<div class="txt">
																	<h2>TOGMAA COMPUND</h2>
																	<p>Cairo <br>town house</p>
																</div>
																<div class="txt2">
																	<ul class="star-list">
																		<li class="active"><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li class="active"><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li class="active"><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																	</ul>
																	<span class="sale">FOR SALE</span>
																</div>
																<span class="price"><span class="add">$</span>226.000</span>
															</div>
														</div>
														<!-- box -->
														<div class="box">
															<div class="img-box">
																<div class="over">
																	<div class="frame">
																		<div class="block">
																			<a href="portfolio-single-image.html" class="view" tabindex="-1"><i class="fa fa-eye"></i></a>
																			<a href="http://creative-wp.com/demos/fekra/images/real-estate/img05.jpg" class="expand lightbox" tabindex="-1"><i class="fa fa-expand"></i></a>
																		</div>
																	</div>
																</div>
																<img src="http://creative-wp.com/demos/fekra/images/real-estate/img05.jpg" alt="image description">
															</div>
															<div class="text-box">
																<div class="txt">
																	<h2>HILLS PLAZA</h2>
																	<p>Cairo <br>town house</p>
																</div>
																<div class="txt2">
																	<ul class="star-list">
																		<li class="active"><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li class="active"><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li class="active"><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																	</ul>
																	<span class="sale">FOR SALE</span>
																</div>
																<span class="price"><span class="add">$</span>158.000</span>
															</div>
														</div>
													</div><div class="beans-slide slick-slide slick-current slick-active" data-slick-index="1" aria-hidden="false" tabindex="-1" role="option" aria-describedby="slick-slide11" style="width: 1170px;">
														<!-- box -->
														<div class="box">
															<div class="img-box">
																<div class="over">
																	<div class="frame">
																		<div class="block">
																			<a href="portfolio-single-image.html" class="view" tabindex="0"><i class="fa fa-eye"></i></a>
																			<a href="http://creative-wp.com/demos/fekra/images/real-estate/img04.jpg" class="expand lightbox" tabindex="0"><i class="fa fa-expand"></i></a>
																		</div>
																	</div>
																</div>
																<img src="http://creative-wp.com/demos/fekra/images/real-estate/img02.jpg" alt="image description">
															</div>
															<div class="text-box">
																<div class="txt">
																	<h2>Akaarat Gadeda</h2>
																	<p>Cairo <br>town house</p>
																</div>
																<div class="txt2">
																	<ul class="star-list">
																		<li class="active"><a href="#" tabindex="0"><i class="fa fa-star"></i></a></li>
																		<li class="active"><a href="#" tabindex="0"><i class="fa fa-star"></i></a></li>
																		<li class="active"><a href="#" tabindex="0"><i class="fa fa-star"></i></a></li>
																		<li><a href="#" tabindex="0"><i class="fa fa-star"></i></a></li>
																		<li><a href="#" tabindex="0"><i class="fa fa-star"></i></a></li>
																	</ul>
																	<span class="sale">FOR SALE</span>
																</div>
																<span class="price"><span class="add">$</span>56.000</span>
															</div>
														</div>
														<!-- box -->
														<div class="box">
															<div class="img-box">
																<div class="over">
																	<div class="frame">
																		<div class="block">
																			<a href="portfolio-single-image.html" class="view" tabindex="0"><i class="fa fa-eye"></i></a>
																			<a href="http://creative-wp.com/demos/fekra/images/real-estate/img02.jpg" class="expand lightbox" tabindex="0"><i class="fa fa-expand"></i></a>
																		</div>
																	</div>
																</div>
																<img src="http://creative-wp.com/demos/fekra/images/real-estate/img03.jpg" alt="image description">
															</div>
															<div class="text-box">
																<div class="txt">
																	<h2>Akaarat Gadeda</h2>
																	<p>Cairo <br>town house</p>
																</div>
																<div class="txt2">
																	<ul class="star-list">
																		<li class="active"><a href="#" tabindex="0"><i class="fa fa-star"></i></a></li>
																		<li class="active"><a href="#" tabindex="0"><i class="fa fa-star"></i></a></li>
																		<li class="active"><a href="#" tabindex="0"><i class="fa fa-star"></i></a></li>
																		<li><a href="#" tabindex="0"><i class="fa fa-star"></i></a></li>
																		<li><a href="#" tabindex="0"><i class="fa fa-star"></i></a></li>
																	</ul>
																	<span class="sale">FOR SALE</span>
																</div>
																<span class="price"><span class="add">$</span>32.000</span>
															</div>
														</div>
														<!-- box -->
														<div class="box">
															<div class="img-box">
																<div class="over">
																	<div class="frame">
																		<div class="block">
																			<a href="portfolio-single-image.html" class="view" tabindex="0"><i class="fa fa-eye"></i></a>
																			<a href="http://creative-wp.com/demos/fekra/images/real-estate/img03.jpg" class="expand lightbox" tabindex="0"><i class="fa fa-expand"></i></a>
																		</div>
																	</div>
																</div>
																<img src="http://creative-wp.com/demos/fekra/images/real-estate/img04.jpg" alt="image description">
															</div>
															<div class="text-box">
																<div class="txt">
																	<h2>TOGMAA COMPUND</h2>
																	<p>Cairo <br>town house</p>
																</div>
																<div class="txt2">
																	<ul class="star-list">
																		<li class="active"><a href="#" tabindex="0"><i class="fa fa-star"></i></a></li>
																		<li class="active"><a href="#" tabindex="0"><i class="fa fa-star"></i></a></li>
																		<li class="active"><a href="#" tabindex="0"><i class="fa fa-star"></i></a></li>
																		<li><a href="#" tabindex="0"><i class="fa fa-star"></i></a></li>
																		<li><a href="#" tabindex="0"><i class="fa fa-star"></i></a></li>
																	</ul>
																	<span class="sale">FOR SALE</span>
																</div>
																<span class="price"><span class="add">$</span>226.000</span>
															</div>
														</div>
														<!-- box -->
														<div class="box">
															<div class="img-box">
																<div class="over">
																	<div class="frame">
																		<div class="block">
																			<a href="portfolio-single-image.html" class="view" tabindex="0"><i class="fa fa-eye"></i></a>
																			<a href="http://creative-wp.com/demos/fekra/images/real-estate/img05.jpg" class="expand lightbox" tabindex="0"><i class="fa fa-expand"></i></a>
																		</div>
																	</div>
																</div>
																<img src="http://creative-wp.com/demos/fekra/images/real-estate/img05.jpg" alt="image description">
															</div>
															<div class="text-box">
																<div class="txt">
																	<h2>HILLS PLAZA</h2>
																	<p>Cairo <br>town house</p>
																</div>
																<div class="txt2">
																	<ul class="star-list">
																		<li class="active"><a href="#" tabindex="0"><i class="fa fa-star"></i></a></li>
																		<li class="active"><a href="#" tabindex="0"><i class="fa fa-star"></i></a></li>
																		<li class="active"><a href="#" tabindex="0"><i class="fa fa-star"></i></a></li>
																		<li><a href="#" tabindex="0"><i class="fa fa-star"></i></a></li>
																		<li><a href="#" tabindex="0"><i class="fa fa-star"></i></a></li>
																	</ul>
																	<span class="sale">FOR SALE</span>
																</div>
																<span class="price"><span class="add">$</span>158.000</span>
															</div>
														</div>
													</div><div class="beans-slide slick-slide slick-cloned" data-slick-index="2" aria-hidden="true" tabindex="-1" style="width: 1170px;">
														<!-- box -->
														<div class="box">
															<div class="img-box">
																<div class="over">
																	<div class="frame">
																		<div class="block">
																			<a href="portfolio-single-image.html" class="view" tabindex="-1"><i class="fa fa-eye"></i></a>
																			<a href="http://creative-wp.com/demos/fekra/images/real-estate/img02.jpg" class="expand lightbox" tabindex="-1"><i class="fa fa-expand"></i></a>
																		</div>
																	</div>
																</div>
																<img src="http://creative-wp.com/demos/fekra/images/real-estate/img02.jpg" alt="image description">
															</div>
															<div class="text-box">
																<div class="txt">
																	<h2>Akaarat Gadeda</h2>
																	<p>Cairo <br>town house</p>
																</div>
																<div class="txt2">
																	<ul class="star-list">
																		<li class="active"><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li class="active"><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li class="active"><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																	</ul>
																	<span class="sale">FOR SALE</span>
																</div>
																<span class="price"><span class="add">$</span>56.000</span>
															</div>
														</div>
														<!-- box -->
														<div class="box">
															<div class="img-box">
																<div class="over">
																	<div class="frame">
																		<div class="block">
																			<a href="portfolio-single-image.html" class="view" tabindex="-1"><i class="fa fa-eye"></i></a>
																			<a href="http://creative-wp.com/demos/fekra/images/real-estate/img03.jpg" class="expand lightbox" tabindex="-1"><i class="fa fa-expand"></i></a>
																		</div>
																	</div>
																</div>
																<img src="http://creative-wp.com/demos/fekra/images/real-estate/img03.jpg" alt="image description">
															</div>
															<div class="text-box">
																<div class="txt">
																	<h2>Akaarat Gadeda</h2>
																	<p>Cairo <br>town house</p>
																</div>
																<div class="txt2">
																	<ul class="star-list">
																		<li class="active"><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li class="active"><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li class="active"><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																	</ul>
																	<span class="sale">FOR SALE</span>
																</div>
																<span class="price"><span class="add">$</span>32.000</span>
															</div>
														</div>
														<!-- box -->
														<div class="box">
															<div class="img-box">
																<div class="over">
																	<div class="frame">
																		<div class="block">
																			<a href="portfolio-single-image.html" class="view" tabindex="-1"><i class="fa fa-eye"></i></a>
																			<a href="http://creative-wp.com/demos/fekra/images/real-estate/img04.jpg" class="expand lightbox" tabindex="-1"><i class="fa fa-expand"></i></a>
																		</div>
																	</div>
																</div>
																<img src="http://creative-wp.com/demos/fekra/images/real-estate/img04.jpg" alt="image description">
															</div>
															<div class="text-box">
																<div class="txt">
																	<h2>TOGMAA COMPUND</h2>
																	<p>Cairo <br>town house</p>
																</div>
																<div class="txt2">
																	<ul class="star-list">
																		<li class="active"><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li class="active"><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li class="active"><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																	</ul>
																	<span class="sale">FOR SALE</span>
																</div>
																<span class="price"><span class="add">$</span>226.000</span>
															</div>
														</div>
														<!-- box -->
														<div class="box">
															<div class="img-box">
																<div class="over">
																	<div class="frame">
																		<div class="block">
																			<a href="portfolio-single-image.html" class="view" tabindex="-1"><i class="fa fa-eye"></i></a>
																			<a href="http://creative-wp.com/demos/fekra/images/real-estate/img05.jpg" class="expand lightbox" tabindex="-1"><i class="fa fa-expand"></i></a>
																		</div>
																	</div>
																</div>
																<img src="http://creative-wp.com/demos/fekra/images/real-estate/img05.jpg" alt="image description">
															</div>
															<div class="text-box">
																<div class="txt">
																	<h2>HILLS PLAZA</h2>
																	<p>Cairo <br>town house</p>
																</div>
																<div class="txt2">
																	<ul class="star-list">
																		<li class="active"><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li class="active"><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li class="active"><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																	</ul>
																	<span class="sale">FOR SALE</span>
																</div>
																<span class="price"><span class="add">$</span>158.000</span>
															</div>
														</div>
													</div></div></div>
													<!-- beans slide -->
													
												<button type="button" data-role="none" class="slick-next slick-arrow" aria-label="Next" role="button" style="display: block;">Next</button></div>
											</div>
										</div>
									</div>
									<div id="tab2-2" class="js-tab-hidden" style="display: none; width: 586px;">
										<!-- beans slider -->
										<div class="beans-slider">
											
											
											<div class="beans-mask">
												<div class="beans-slideset slick-initialized slick-slider"><button type="button" data-role="none" class="slick-prev slick-arrow" aria-label="Previous" role="button" style="display: block;">Previous</button>
													<!-- beans slide -->
													<div aria-live="polite" class="slick-list draggable"><div class="slick-track" role="listbox" style="opacity: 1; width: 2344px; left: -1172px;"><div class="beans-slide slick-slide slick-cloned" data-slick-index="-1" aria-hidden="true" tabindex="-1" style="width: 586px;">
														<!-- box -->
														<div class="box">
															<div class="img-box">
																<div class="over">
																	<div class="frame">
																		<div class="block">
																			<a href="portfolio-single-image.html" class="view" tabindex="-1"><i class="fa fa-eye"></i></a>
																			<a href="http://creative-wp.com/demos/fekra/images/real-estate/img04.jpg" class="expand lightbox" tabindex="-1"><i class="fa fa-expand"></i></a>
																		</div>
																	</div>
																</div>
																<img src="http://creative-wp.com/demos/fekra/images/real-estate/img04.jpg" alt="image description">
															</div>
															<div class="text-box">
																<div class="txt">
																	<h2>TOGMAA COMPUND</h2>
																	<p>Cairo <br>town house</p>
																</div>
																<div class="txt2">
																	<ul class="star-list">
																		<li class="active"><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li class="active"><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li class="active"><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																	</ul>
																	<span class="sale">FOR SALE</span>
																</div>
																<span class="price"><span class="add">$</span>226.000</span>
															</div>
														</div>
														<!-- box -->
														<div class="box">
															<div class="img-box">
																<div class="over">
																	<div class="frame">
																		<div class="block">
																			<a href="portfolio-single-image.html" class="view" tabindex="-1"><i class="fa fa-eye"></i></a>
																			<a href="http://creative-wp.com/demos/fekra/images/real-estate/img02.jpg" class="expand lightbox" tabindex="-1"><i class="fa fa-expand"></i></a>
																		</div>
																	</div>
																</div>
																<img src="http://creative-wp.com/demos/fekra/images/real-estate/img02.jpg" alt="image description">
															</div>
															<div class="text-box">
																<div class="txt">
																	<h2>Akaarat Gadeda</h2>
																	<p>Cairo <br>town house</p>
																</div>
																<div class="txt2">
																	<ul class="star-list">
																		<li class="active"><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li class="active"><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li class="active"><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																	</ul>
																	<span class="sale">FOR SALE</span>
																</div>
																<span class="price"><span class="add">$</span>56.000</span>
															</div>
														</div>
														<!-- box -->
														<div class="box">
															<div class="img-box">
																<div class="over">
																	<div class="frame">
																		<div class="block">
																			<a href="portfolio-single-image.html" class="view" tabindex="-1"><i class="fa fa-eye"></i></a>
																			<a href="http://creative-wp.com/demos/fekra/images/real-estate/img05.jpg" class="expand lightbox" tabindex="-1"><i class="fa fa-expand"></i></a>
																		</div>
																	</div>
																</div>
																<img src="http://creative-wp.com/demos/fekra/images/real-estate/img05.jpg" alt="image description">
															</div>
															<div class="text-box">
																<div class="txt">
																	<h2>HILLS PLAZA</h2>
																	<p>Cairo <br>town house</p>
																</div>
																<div class="txt2">
																	<ul class="star-list">
																		<li class="active"><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li class="active"><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li class="active"><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																	</ul>
																	<span class="sale">FOR SALE</span>
																</div>
																<span class="price"><span class="add">$</span>158.000</span>
															</div>
														</div>
														<!-- box -->
														<div class="box">
															<div class="img-box">
																<div class="over">
																	<div class="frame">
																		<div class="block">
																			<a href="portfolio-single-image.html" class="view" tabindex="-1"><i class="fa fa-eye"></i></a>
																			<a href="http://creative-wp.com/demos/fekra/images/real-estate/img03.jpg" class="expand lightbox" tabindex="-1"><i class="fa fa-expand"></i></a>
																		</div>
																	</div>
																</div>
																<img src="http://creative-wp.com/demos/fekra/images/real-estate/img03.jpg" alt="image description">
															</div>
															<div class="text-box">
																<div class="txt">
																	<h2>Akaarat Gadeda</h2>
																	<p>Cairo <br>town house</p>
																</div>
																<div class="txt2">
																	<ul class="star-list">
																		<li class="active"><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li class="active"><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li class="active"><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																	</ul>
																	<span class="sale">FOR SALE</span>
																</div>
																<span class="price"><span class="add">$</span>32.000</span>
															</div>
														</div>
													</div><div class="beans-slide slick-slide" data-slick-index="0" aria-hidden="true" tabindex="-1" role="option" aria-describedby="slick-slide20" style="width: 586px;">
														<!-- box -->
														<div class="box">
															<div class="img-box">
																<div class="over">
																	<div class="frame">
																		<div class="block">
																			<a href="portfolio-single-image.html" class="view" tabindex="-1"><i class="fa fa-eye"></i></a>
																			<a href="http://creative-wp.com/demos/fekra/images/real-estate/img04.jpg" class="expand lightbox" tabindex="-1"><i class="fa fa-expand"></i></a>
																		</div>
																	</div>
																</div>
																<img src="http://creative-wp.com/demos/fekra/images/real-estate/img04.jpg" alt="image description">
															</div>
															<div class="text-box">
																<div class="txt">
																	<h2>TOGMAA COMPUND</h2>
																	<p>Cairo <br>town house</p>
																</div>
																<div class="txt2">
																	<ul class="star-list">
																		<li class="active"><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li class="active"><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li class="active"><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																	</ul>
																	<span class="sale">FOR SALE</span>
																</div>
																<span class="price"><span class="add">$</span>226.000</span>
															</div>
														</div>
														<!-- box -->
														<div class="box">
															<div class="img-box">
																<div class="over">
																	<div class="frame">
																		<div class="block">
																			<a href="portfolio-single-image.html" class="view" tabindex="-1"><i class="fa fa-eye"></i></a>
																			<a href="http://creative-wp.com/demos/fekra/images/real-estate/img02.jpg" class="expand lightbox" tabindex="-1"><i class="fa fa-expand"></i></a>
																		</div>
																	</div>
																</div>
																<img src="http://creative-wp.com/demos/fekra/images/real-estate/img02.jpg" alt="image description">
															</div>
															<div class="text-box">
																<div class="txt">
																	<h2>Akaarat Gadeda</h2>
																	<p>Cairo <br>town house</p>
																</div>
																<div class="txt2">
																	<ul class="star-list">
																		<li class="active"><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li class="active"><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li class="active"><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																	</ul>
																	<span class="sale">FOR SALE</span>
																</div>
																<span class="price"><span class="add">$</span>56.000</span>
															</div>
														</div>
														<!-- box -->
														<div class="box">
															<div class="img-box">
																<div class="over">
																	<div class="frame">
																		<div class="block">
																			<a href="portfolio-single-image.html" class="view" tabindex="-1"><i class="fa fa-eye"></i></a>
																			<a href="http://creative-wp.com/demos/fekra/images/real-estate/img05.jpg" class="expand lightbox" tabindex="-1"><i class="fa fa-expand"></i></a>
																		</div>
																	</div>
																</div>
																<img src="http://creative-wp.com/demos/fekra/images/real-estate/img05.jpg" alt="image description">
															</div>
															<div class="text-box">
																<div class="txt">
																	<h2>HILLS PLAZA</h2>
																	<p>Cairo <br>town house</p>
																</div>
																<div class="txt2">
																	<ul class="star-list">
																		<li class="active"><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li class="active"><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li class="active"><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																	</ul>
																	<span class="sale">FOR SALE</span>
																</div>
																<span class="price"><span class="add">$</span>158.000</span>
															</div>
														</div>
														<!-- box -->
														<div class="box">
															<div class="img-box">
																<div class="over">
																	<div class="frame">
																		<div class="block">
																			<a href="portfolio-single-image.html" class="view" tabindex="-1"><i class="fa fa-eye"></i></a>
																			<a href="http://creative-wp.com/demos/fekra/images/real-estate/img03.jpg" class="expand lightbox" tabindex="-1"><i class="fa fa-expand"></i></a>
																		</div>
																	</div>
																</div>
																<img src="http://creative-wp.com/demos/fekra/images/real-estate/img03.jpg" alt="image description">
															</div>
															<div class="text-box">
																<div class="txt">
																	<h2>Akaarat Gadeda</h2>
																	<p>Cairo <br>town house</p>
																</div>
																<div class="txt2">
																	<ul class="star-list">
																		<li class="active"><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li class="active"><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li class="active"><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																	</ul>
																	<span class="sale">FOR SALE</span>
																</div>
																<span class="price"><span class="add">$</span>32.000</span>
															</div>
														</div>
													</div><div class="beans-slide slick-slide slick-current slick-active" data-slick-index="1" aria-hidden="false" tabindex="-1" role="option" aria-describedby="slick-slide21" style="width: 586px;">
														<!-- box -->
														<div class="box">
															<div class="img-box">
																<div class="over">
																	<div class="frame">
																		<div class="block">
																			<a href="portfolio-single-image.html" class="view" tabindex="0"><i class="fa fa-eye"></i></a>
																			<a href="http://creative-wp.com/demos/fekra/images/real-estate/img04.jpg" class="expand lightbox" tabindex="0"><i class="fa fa-expand"></i></a>
																		</div>
																	</div>
																</div>
																<img src="http://creative-wp.com/demos/fekra/images/real-estate/img04.jpg" alt="image description">
															</div>
															<div class="text-box">
																<div class="txt">
																	<h2>TOGMAA COMPUND</h2>
																	<p>Cairo <br>town house</p>
																</div>
																<div class="txt2">
																	<ul class="star-list">
																		<li class="active"><a href="#" tabindex="0"><i class="fa fa-star"></i></a></li>
																		<li class="active"><a href="#" tabindex="0"><i class="fa fa-star"></i></a></li>
																		<li class="active"><a href="#" tabindex="0"><i class="fa fa-star"></i></a></li>
																		<li><a href="#" tabindex="0"><i class="fa fa-star"></i></a></li>
																		<li><a href="#" tabindex="0"><i class="fa fa-star"></i></a></li>
																	</ul>
																	<span class="sale">FOR SALE</span>
																</div>
																<span class="price"><span class="add">$</span>226.000</span>
															</div>
														</div>
														<!-- box -->
														<div class="box">
															<div class="img-box">
																<div class="over">
																	<div class="frame">
																		<div class="block">
																			<a href="portfolio-single-image.html" class="view" tabindex="0"><i class="fa fa-eye"></i></a>
																			<a href="http://creative-wp.com/demos/fekra/images/real-estate/img02.jpg" class="expand lightbox" tabindex="0"><i class="fa fa-expand"></i></a>
																		</div>
																	</div>
																</div>
																<img src="http://creative-wp.com/demos/fekra/images/real-estate/img02.jpg" alt="image description">
															</div>
															<div class="text-box">
																<div class="txt">
																	<h2>Akaarat Gadeda</h2>
																	<p>Cairo <br>town house</p>
																</div>
																<div class="txt2">
																	<ul class="star-list">
																		<li class="active"><a href="#" tabindex="0"><i class="fa fa-star"></i></a></li>
																		<li class="active"><a href="#" tabindex="0"><i class="fa fa-star"></i></a></li>
																		<li class="active"><a href="#" tabindex="0"><i class="fa fa-star"></i></a></li>
																		<li><a href="#" tabindex="0"><i class="fa fa-star"></i></a></li>
																		<li><a href="#" tabindex="0"><i class="fa fa-star"></i></a></li>
																	</ul>
																	<span class="sale">FOR SALE</span>
																</div>
																<span class="price"><span class="add">$</span>56.000</span>
															</div>
														</div>
														<!-- box -->
														<div class="box">
															<div class="img-box">
																<div class="over">
																	<div class="frame">
																		<div class="block">
																			<a href="portfolio-single-image.html" class="view" tabindex="0"><i class="fa fa-eye"></i></a>
																			<a href="http://creative-wp.com/demos/fekra/images/real-estate/img05.jpg" class="expand lightbox" tabindex="0"><i class="fa fa-expand"></i></a>
																		</div>
																	</div>
																</div>
																<img src="http://creative-wp.com/demos/fekra/images/real-estate/img05.jpg" alt="image description">
															</div>
															<div class="text-box">
																<div class="txt">
																	<h2>HILLS PLAZA</h2>
																	<p>Cairo <br>town house</p>
																</div>
																<div class="txt2">
																	<ul class="star-list">
																		<li class="active"><a href="#" tabindex="0"><i class="fa fa-star"></i></a></li>
																		<li class="active"><a href="#" tabindex="0"><i class="fa fa-star"></i></a></li>
																		<li class="active"><a href="#" tabindex="0"><i class="fa fa-star"></i></a></li>
																		<li><a href="#" tabindex="0"><i class="fa fa-star"></i></a></li>
																		<li><a href="#" tabindex="0"><i class="fa fa-star"></i></a></li>
																	</ul>
																	<span class="sale">FOR SALE</span>
																</div>
																<span class="price"><span class="add">$</span>158.000</span>
															</div>
														</div>
														<!-- box -->
														<div class="box">
															<div class="img-box">
																<div class="over">
																	<div class="frame">
																		<div class="block">
																			<a href="portfolio-single-image.html" class="view" tabindex="0"><i class="fa fa-eye"></i></a>
																			<a href="http://creative-wp.com/demos/fekra/images/real-estate/img03.jpg" class="expand lightbox" tabindex="0"><i class="fa fa-expand"></i></a>
																		</div>
																	</div>
																</div>
																<img src="http://creative-wp.com/demos/fekra/images/real-estate/img03.jpg" alt="image description">
															</div>
															<div class="text-box">
																<div class="txt">
																	<h2>Akaarat Gadeda</h2>
																	<p>Cairo <br>town house</p>
																</div>
																<div class="txt2">
																	<ul class="star-list">
																		<li class="active"><a href="#" tabindex="0"><i class="fa fa-star"></i></a></li>
																		<li class="active"><a href="#" tabindex="0"><i class="fa fa-star"></i></a></li>
																		<li class="active"><a href="#" tabindex="0"><i class="fa fa-star"></i></a></li>
																		<li><a href="#" tabindex="0"><i class="fa fa-star"></i></a></li>
																		<li><a href="#" tabindex="0"><i class="fa fa-star"></i></a></li>
																	</ul>
																	<span class="sale">FOR SALE</span>
																</div>
																<span class="price"><span class="add">$</span>32.000</span>
															</div>
														</div>
													</div><div class="beans-slide slick-slide slick-cloned" data-slick-index="2" aria-hidden="true" tabindex="-1" style="width: 586px;">
														<!-- box -->
														<div class="box">
															<div class="img-box">
																<div class="over">
																	<div class="frame">
																		<div class="block">
																			<a href="portfolio-single-image.html" class="view" tabindex="-1"><i class="fa fa-eye"></i></a>
																			<a href="http://creative-wp.com/demos/fekra/images/real-estate/img04.jpg" class="expand lightbox" tabindex="-1"><i class="fa fa-expand"></i></a>
																		</div>
																	</div>
																</div>
																<img src="http://creative-wp.com/demos/fekra/images/real-estate/img04.jpg" alt="image description">
															</div>
															<div class="text-box">
																<div class="txt">
																	<h2>TOGMAA COMPUND</h2>
																	<p>Cairo <br>town house</p>
																</div>
																<div class="txt2">
																	<ul class="star-list">
																		<li class="active"><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li class="active"><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li class="active"><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																	</ul>
																	<span class="sale">FOR SALE</span>
																</div>
																<span class="price"><span class="add">$</span>226.000</span>
															</div>
														</div>
														<!-- box -->
														<div class="box">
															<div class="img-box">
																<div class="over">
																	<div class="frame">
																		<div class="block">
																			<a href="portfolio-single-image.html" class="view" tabindex="-1"><i class="fa fa-eye"></i></a>
																			<a href="http://creative-wp.com/demos/fekra/images/real-estate/img02.jpg" class="expand lightbox" tabindex="-1"><i class="fa fa-expand"></i></a>
																		</div>
																	</div>
																</div>
																<img src="http://creative-wp.com/demos/fekra/images/real-estate/img02.jpg" alt="image description">
															</div>
															<div class="text-box">
																<div class="txt">
																	<h2>Akaarat Gadeda</h2>
																	<p>Cairo <br>town house</p>
																</div>
																<div class="txt2">
																	<ul class="star-list">
																		<li class="active"><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li class="active"><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li class="active"><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																	</ul>
																	<span class="sale">FOR SALE</span>
																</div>
																<span class="price"><span class="add">$</span>56.000</span>
															</div>
														</div>
														<!-- box -->
														<div class="box">
															<div class="img-box">
																<div class="over">
																	<div class="frame">
																		<div class="block">
																			<a href="portfolio-single-image.html" class="view" tabindex="-1"><i class="fa fa-eye"></i></a>
																			<a href="http://creative-wp.com/demos/fekra/images/real-estate/img05.jpg" class="expand lightbox" tabindex="-1"><i class="fa fa-expand"></i></a>
																		</div>
																	</div>
																</div>
																<img src="http://creative-wp.com/demos/fekra/images/real-estate/img05.jpg" alt="image description">
															</div>
															<div class="text-box">
																<div class="txt">
																	<h2>HILLS PLAZA</h2>
																	<p>Cairo <br>town house</p>
																</div>
																<div class="txt2">
																	<ul class="star-list">
																		<li class="active"><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li class="active"><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li class="active"><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																	</ul>
																	<span class="sale">FOR SALE</span>
																</div>
																<span class="price"><span class="add">$</span>158.000</span>
															</div>
														</div>
														<!-- box -->
														<div class="box">
															<div class="img-box">
																<div class="over">
																	<div class="frame">
																		<div class="block">
																			<a href="portfolio-single-image.html" class="view" tabindex="-1"><i class="fa fa-eye"></i></a>
																			<a href="http://creative-wp.com/demos/fekra/images/real-estate/img03.jpg" class="expand lightbox" tabindex="-1"><i class="fa fa-expand"></i></a>
																		</div>
																	</div>
																</div>
																<img src="http://creative-wp.com/demos/fekra/images/real-estate/img03.jpg" alt="image description">
															</div>
															<div class="text-box">
																<div class="txt">
																	<h2>Akaarat Gadeda</h2>
																	<p>Cairo <br>town house</p>
																</div>
																<div class="txt2">
																	<ul class="star-list">
																		<li class="active"><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li class="active"><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li class="active"><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																	</ul>
																	<span class="sale">FOR SALE</span>
																</div>
																<span class="price"><span class="add">$</span>32.000</span>
															</div>
														</div>
													</div></div></div>
													<!-- beans slide -->
													
												<button type="button" data-role="none" class="slick-next slick-arrow" aria-label="Next" role="button" style="display: block;">Next</button></div>
											</div>
										</div>
									</div>
									<div id="tab3-2" class="js-tab-hidden" style="display: none; width: 586px;">
										<div class="beans-slider">
											
											
											<div class="beans-mask">
												<div class="beans-slideset slick-initialized slick-slider"><button type="button" data-role="none" class="slick-prev slick-arrow" aria-label="Previous" role="button" style="display: block;">Previous</button>
													<!-- beans slide -->
													<div aria-live="polite" class="slick-list draggable"><div class="slick-track" role="listbox" style="opacity: 1; width: 2344px; left: -1172px;"><div class="beans-slide slick-slide slick-cloned" data-slick-index="-1" aria-hidden="true" tabindex="-1" style="width: 586px;">
														<!-- box -->
														<div class="box">
															<div class="img-box">
																<div class="over">
																	<div class="frame">
																		<div class="block">
																			<a href="portfolio-single-image.html" class="view" tabindex="-1"><i class="fa fa-eye"></i></a>
																			<a href="http://creative-wp.com/demos/fekra/images/real-estate/img03.jpg" class="expand lightbox" tabindex="-1"><i class="fa fa-expand"></i></a>
																		</div>
																	</div>
																</div>
																<img src="http://creative-wp.com/demos/fekra/images/real-estate/img03.jpg" alt="image description">
															</div>
															<div class="text-box">
																<div class="txt">
																	<h2>Akaarat Gadeda</h2>
																	<p>Cairo <br>town house</p>
																</div>
																<div class="txt2">
																	<ul class="star-list">
																		<li class="active"><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li class="active"><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li class="active"><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																	</ul>
																	<span class="sale">FOR SALE</span>
																</div>
																<span class="price"><span class="add">$</span>32.000</span>
															</div>
														</div>
														<!-- box -->
														<div class="box">
															<div class="img-box">
																<div class="over">
																	<div class="frame">
																		<div class="block">
																			<a href="portfolio-single-image.html" class="view" tabindex="-1"><i class="fa fa-eye"></i></a>
																			<a href="http://creative-wp.com/demos/fekra/images/real-estate/img02.jpg" class="expand lightbox" tabindex="-1"><i class="fa fa-expand"></i></a>
																		</div>
																	</div>
																</div>
																<img src="http://creative-wp.com/demos/fekra/images/real-estate/img02.jpg" alt="image description">
															</div>
															<div class="text-box">
																<div class="txt">
																	<h2>Akaarat Gadeda</h2>
																	<p>Cairo <br>town house</p>
																</div>
																<div class="txt2">
																	<ul class="star-list">
																		<li class="active"><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li class="active"><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li class="active"><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																	</ul>
																	<span class="sale">FOR SALE</span>
																</div>
																<span class="price"><span class="add">$</span>56.000</span>
															</div>
														</div>
														<!-- box -->
														<div class="box">
															<div class="img-box">
																<div class="over">
																	<div class="frame">
																		<div class="block">
																			<a href="portfolio-single-image.html" class="view" tabindex="-1"><i class="fa fa-eye"></i></a>
																			<a href="http://creative-wp.com/demos/fekra/images/real-estate/img05.jpg" class="expand lightbox" tabindex="-1"><i class="fa fa-expand"></i></a>
																		</div>
																	</div>
																</div>
																<img src="http://creative-wp.com/demos/fekra/images/real-estate/img05.jpg" alt="image description">
															</div>
															<div class="text-box">
																<div class="txt">
																	<h2>HILLS PLAZA</h2>
																	<p>Cairo <br>town house</p>
																</div>
																<div class="txt2">
																	<ul class="star-list">
																		<li class="active"><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li class="active"><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li class="active"><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																	</ul>
																	<span class="sale">FOR SALE</span>
																</div>
																<span class="price"><span class="add">$</span>158.000</span>
															</div>
														</div>
														<!-- box -->
														<div class="box">
															<div class="img-box">
																<div class="over">
																	<div class="frame">
																		<div class="block">
																			<a href="portfolio-single-image.html" class="view" tabindex="-1"><i class="fa fa-eye"></i></a>
																			<a href="http://creative-wp.com/demos/fekra/images/real-estate/img04.jpg" class="expand lightbox" tabindex="-1"><i class="fa fa-expand"></i></a>
																		</div>
																	</div>
																</div>
																<img src="http://creative-wp.com/demos/fekra/images/real-estate/img04.jpg" alt="image description">
															</div>
															<div class="text-box">
																<div class="txt">
																	<h2>TOGMAA COMPUND</h2>
																	<p>Cairo <br>town house</p>
																</div>
																<div class="txt2">
																	<ul class="star-list">
																		<li class="active"><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li class="active"><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li class="active"><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																	</ul>
																	<span class="sale">FOR SALE</span>
																</div>
																<span class="price"><span class="add">$</span>226.000</span>
															</div>
														</div>
													</div><div class="beans-slide slick-slide" data-slick-index="0" aria-hidden="true" tabindex="-1" role="option" aria-describedby="slick-slide30" style="width: 586px;">
														<!-- box -->
														<div class="box">
															<div class="img-box">
																<div class="over">
																	<div class="frame">
																		<div class="block">
																			<a href="portfolio-single-image.html" class="view" tabindex="-1"><i class="fa fa-eye"></i></a>
																			<a href="http://creative-wp.com/demos/fekra/images/real-estate/img03.jpg" class="expand lightbox" tabindex="-1"><i class="fa fa-expand"></i></a>
																		</div>
																	</div>
																</div>
																<img src="http://creative-wp.com/demos/fekra/images/real-estate/img03.jpg" alt="image description">
															</div>
															<div class="text-box">
																<div class="txt">
																	<h2>Akaarat Gadeda</h2>
																	<p>Cairo <br>town house</p>
																</div>
																<div class="txt2">
																	<ul class="star-list">
																		<li class="active"><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li class="active"><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li class="active"><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																	</ul>
																	<span class="sale">FOR SALE</span>
																</div>
																<span class="price"><span class="add">$</span>32.000</span>
															</div>
														</div>
														<!-- box -->
														<div class="box">
															<div class="img-box">
																<div class="over">
																	<div class="frame">
																		<div class="block">
																			<a href="portfolio-single-image.html" class="view" tabindex="-1"><i class="fa fa-eye"></i></a>
																			<a href="http://creative-wp.com/demos/fekra/images/real-estate/img02.jpg" class="expand lightbox" tabindex="-1"><i class="fa fa-expand"></i></a>
																		</div>
																	</div>
																</div>
																<img src="http://creative-wp.com/demos/fekra/images/real-estate/img02.jpg" alt="image description">
															</div>
															<div class="text-box">
																<div class="txt">
																	<h2>Akaarat Gadeda</h2>
																	<p>Cairo <br>town house</p>
																</div>
																<div class="txt2">
																	<ul class="star-list">
																		<li class="active"><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li class="active"><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li class="active"><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																	</ul>
																	<span class="sale">FOR SALE</span>
																</div>
																<span class="price"><span class="add">$</span>56.000</span>
															</div>
														</div>
														<!-- box -->
														<div class="box">
															<div class="img-box">
																<div class="over">
																	<div class="frame">
																		<div class="block">
																			<a href="portfolio-single-image.html" class="view" tabindex="-1"><i class="fa fa-eye"></i></a>
																			<a href="http://creative-wp.com/demos/fekra/images/real-estate/img05.jpg" class="expand lightbox" tabindex="-1"><i class="fa fa-expand"></i></a>
																		</div>
																	</div>
																</div>
																<img src="http://creative-wp.com/demos/fekra/images/real-estate/img05.jpg" alt="image description">
															</div>
															<div class="text-box">
																<div class="txt">
																	<h2>HILLS PLAZA</h2>
																	<p>Cairo <br>town house</p>
																</div>
																<div class="txt2">
																	<ul class="star-list">
																		<li class="active"><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li class="active"><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li class="active"><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																	</ul>
																	<span class="sale">FOR SALE</span>
																</div>
																<span class="price"><span class="add">$</span>158.000</span>
															</div>
														</div>
														<!-- box -->
														<div class="box">
															<div class="img-box">
																<div class="over">
																	<div class="frame">
																		<div class="block">
																			<a href="portfolio-single-image.html" class="view" tabindex="-1"><i class="fa fa-eye"></i></a>
																			<a href="http://creative-wp.com/demos/fekra/images/real-estate/img04.jpg" class="expand lightbox" tabindex="-1"><i class="fa fa-expand"></i></a>
																		</div>
																	</div>
																</div>
																<img src="http://creative-wp.com/demos/fekra/images/real-estate/img04.jpg" alt="image description">
															</div>
															<div class="text-box">
																<div class="txt">
																	<h2>TOGMAA COMPUND</h2>
																	<p>Cairo <br>town house</p>
																</div>
																<div class="txt2">
																	<ul class="star-list">
																		<li class="active"><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li class="active"><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li class="active"><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																	</ul>
																	<span class="sale">FOR SALE</span>
																</div>
																<span class="price"><span class="add">$</span>226.000</span>
															</div>
														</div>
													</div><div class="beans-slide slick-slide slick-current slick-active" data-slick-index="1" aria-hidden="false" tabindex="-1" role="option" aria-describedby="slick-slide31" style="width: 586px;">
														<!-- box -->
														<div class="box">
															<div class="img-box">
																<div class="over">
																	<div class="frame">
																		<div class="block">
																			<a href="portfolio-single-image.html" class="view" tabindex="0"><i class="fa fa-eye"></i></a>
																			<a href="http://creative-wp.com/demos/fekra/images/real-estate/img03.jpg" class="expand lightbox" tabindex="0"><i class="fa fa-expand"></i></a>
																		</div>
																	</div>
																</div>
																<img src="http://creative-wp.com/demos/fekra/images/real-estate/img03.jpg" alt="image description">
															</div>
															<div class="text-box">
																<div class="txt">
																	<h2>Akaarat Gadeda</h2>
																	<p>Cairo <br>town house</p>
																</div>
																<div class="txt2">
																	<ul class="star-list">
																		<li class="active"><a href="#" tabindex="0"><i class="fa fa-star"></i></a></li>
																		<li class="active"><a href="#" tabindex="0"><i class="fa fa-star"></i></a></li>
																		<li class="active"><a href="#" tabindex="0"><i class="fa fa-star"></i></a></li>
																		<li><a href="#" tabindex="0"><i class="fa fa-star"></i></a></li>
																		<li><a href="#" tabindex="0"><i class="fa fa-star"></i></a></li>
																	</ul>
																	<span class="sale">FOR SALE</span>
																</div>
																<span class="price"><span class="add">$</span>32.000</span>
															</div>
														</div>
														<!-- box -->
														<div class="box">
															<div class="img-box">
																<div class="over">
																	<div class="frame">
																		<div class="block">
																			<a href="portfolio-single-image.html" class="view" tabindex="0"><i class="fa fa-eye"></i></a>
																			<a href="http://creative-wp.com/demos/fekra/images/real-estate/img02.jpg" class="expand lightbox" tabindex="0"><i class="fa fa-expand"></i></a>
																		</div>
																	</div>
																</div>
																<img src="http://creative-wp.com/demos/fekra/images/real-estate/img02.jpg" alt="image description">
															</div>
															<div class="text-box">
																<div class="txt">
																	<h2>Akaarat Gadeda</h2>
																	<p>Cairo <br>town house</p>
																</div>
																<div class="txt2">
																	<ul class="star-list">
																		<li class="active"><a href="#" tabindex="0"><i class="fa fa-star"></i></a></li>
																		<li class="active"><a href="#" tabindex="0"><i class="fa fa-star"></i></a></li>
																		<li class="active"><a href="#" tabindex="0"><i class="fa fa-star"></i></a></li>
																		<li><a href="#" tabindex="0"><i class="fa fa-star"></i></a></li>
																		<li><a href="#" tabindex="0"><i class="fa fa-star"></i></a></li>
																	</ul>
																	<span class="sale">FOR SALE</span>
																</div>
																<span class="price"><span class="add">$</span>56.000</span>
															</div>
														</div>
														<!-- box -->
														<div class="box">
															<div class="img-box">
																<div class="over">
																	<div class="frame">
																		<div class="block">
																			<a href="portfolio-single-image.html" class="view" tabindex="0"><i class="fa fa-eye"></i></a>
																			<a href="http://creative-wp.com/demos/fekra/images/real-estate/img05.jpg" class="expand lightbox" tabindex="0"><i class="fa fa-expand"></i></a>
																		</div>
																	</div>
																</div>
																<img src="http://creative-wp.com/demos/fekra/images/real-estate/img05.jpg" alt="image description">
															</div>
															<div class="text-box">
																<div class="txt">
																	<h2>HILLS PLAZA</h2>
																	<p>Cairo <br>town house</p>
																</div>
																<div class="txt2">
																	<ul class="star-list">
																		<li class="active"><a href="#" tabindex="0"><i class="fa fa-star"></i></a></li>
																		<li class="active"><a href="#" tabindex="0"><i class="fa fa-star"></i></a></li>
																		<li class="active"><a href="#" tabindex="0"><i class="fa fa-star"></i></a></li>
																		<li><a href="#" tabindex="0"><i class="fa fa-star"></i></a></li>
																		<li><a href="#" tabindex="0"><i class="fa fa-star"></i></a></li>
																	</ul>
																	<span class="sale">FOR SALE</span>
																</div>
																<span class="price"><span class="add">$</span>158.000</span>
															</div>
														</div>
														<!-- box -->
														<div class="box">
															<div class="img-box">
																<div class="over">
																	<div class="frame">
																		<div class="block">
																			<a href="portfolio-single-image.html" class="view" tabindex="0"><i class="fa fa-eye"></i></a>
																			<a href="http://creative-wp.com/demos/fekra/images/real-estate/img04.jpg" class="expand lightbox" tabindex="0"><i class="fa fa-expand"></i></a>
																		</div>
																	</div>
																</div>
																<img src="http://creative-wp.com/demos/fekra/images/real-estate/img04.jpg" alt="image description">
															</div>
															<div class="text-box">
																<div class="txt">
																	<h2>TOGMAA COMPUND</h2>
																	<p>Cairo <br>town house</p>
																</div>
																<div class="txt2">
																	<ul class="star-list">
																		<li class="active"><a href="#" tabindex="0"><i class="fa fa-star"></i></a></li>
																		<li class="active"><a href="#" tabindex="0"><i class="fa fa-star"></i></a></li>
																		<li class="active"><a href="#" tabindex="0"><i class="fa fa-star"></i></a></li>
																		<li><a href="#" tabindex="0"><i class="fa fa-star"></i></a></li>
																		<li><a href="#" tabindex="0"><i class="fa fa-star"></i></a></li>
																	</ul>
																	<span class="sale">FOR SALE</span>
																</div>
																<span class="price"><span class="add">$</span>226.000</span>
															</div>
														</div>
													</div><div class="beans-slide slick-slide slick-cloned" data-slick-index="2" aria-hidden="true" tabindex="-1" style="width: 586px;">
														<!-- box -->
														<div class="box">
															<div class="img-box">
																<div class="over">
																	<div class="frame">
																		<div class="block">
																			<a href="portfolio-single-image.html" class="view" tabindex="-1"><i class="fa fa-eye"></i></a>
																			<a href="http://creative-wp.com/demos/fekra/images/real-estate/img03.jpg" class="expand lightbox" tabindex="-1"><i class="fa fa-expand"></i></a>
																		</div>
																	</div>
																</div>
																<img src="http://creative-wp.com/demos/fekra/images/real-estate/img03.jpg" alt="image description">
															</div>
															<div class="text-box">
																<div class="txt">
																	<h2>Akaarat Gadeda</h2>
																	<p>Cairo <br>town house</p>
																</div>
																<div class="txt2">
																	<ul class="star-list">
																		<li class="active"><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li class="active"><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li class="active"><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																	</ul>
																	<span class="sale">FOR SALE</span>
																</div>
																<span class="price"><span class="add">$</span>32.000</span>
															</div>
														</div>
														<!-- box -->
														<div class="box">
															<div class="img-box">
																<div class="over">
																	<div class="frame">
																		<div class="block">
																			<a href="portfolio-single-image.html" class="view" tabindex="-1"><i class="fa fa-eye"></i></a>
																			<a href="http://creative-wp.com/demos/fekra/images/real-estate/img02.jpg" class="expand lightbox" tabindex="-1"><i class="fa fa-expand"></i></a>
																		</div>
																	</div>
																</div>
																<img src="http://creative-wp.com/demos/fekra/images/real-estate/img02.jpg" alt="image description">
															</div>
															<div class="text-box">
																<div class="txt">
																	<h2>Akaarat Gadeda</h2>
																	<p>Cairo <br>town house</p>
																</div>
																<div class="txt2">
																	<ul class="star-list">
																		<li class="active"><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li class="active"><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li class="active"><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																	</ul>
																	<span class="sale">FOR SALE</span>
																</div>
																<span class="price"><span class="add">$</span>56.000</span>
															</div>
														</div>
														<!-- box -->
														<div class="box">
															<div class="img-box">
																<div class="over">
																	<div class="frame">
																		<div class="block">
																			<a href="portfolio-single-image.html" class="view" tabindex="-1"><i class="fa fa-eye"></i></a>
																			<a href="http://creative-wp.com/demos/fekra/images/real-estate/img05.jpg" class="expand lightbox" tabindex="-1"><i class="fa fa-expand"></i></a>
																		</div>
																	</div>
																</div>
																<img src="http://creative-wp.com/demos/fekra/images/real-estate/img05.jpg" alt="image description">
															</div>
															<div class="text-box">
																<div class="txt">
																	<h2>HILLS PLAZA</h2>
																	<p>Cairo <br>town house</p>
																</div>
																<div class="txt2">
																	<ul class="star-list">
																		<li class="active"><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li class="active"><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li class="active"><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																	</ul>
																	<span class="sale">FOR SALE</span>
																</div>
																<span class="price"><span class="add">$</span>158.000</span>
															</div>
														</div>
														<!-- box -->
														<div class="box">
															<div class="img-box">
																<div class="over">
																	<div class="frame">
																		<div class="block">
																			<a href="portfolio-single-image.html" class="view" tabindex="-1"><i class="fa fa-eye"></i></a>
																			<a href="http://creative-wp.com/demos/fekra/images/real-estate/img04.jpg" class="expand lightbox" tabindex="-1"><i class="fa fa-expand"></i></a>
																		</div>
																	</div>
																</div>
																<img src="http://creative-wp.com/demos/fekra/images/real-estate/img04.jpg" alt="image description">
															</div>
															<div class="text-box">
																<div class="txt">
																	<h2>TOGMAA COMPUND</h2>
																	<p>Cairo <br>town house</p>
																</div>
																<div class="txt2">
																	<ul class="star-list">
																		<li class="active"><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li class="active"><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li class="active"><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																		<li><a href="#" tabindex="-1"><i class="fa fa-star"></i></a></li>
																	</ul>
																	<span class="sale">FOR SALE</span>
																</div>
																<span class="price"><span class="add">$</span>226.000</span>
															</div>
														</div>
													</div></div></div>
													<!-- beans slide -->
													
												<button type="button" data-role="none" class="slick-next slick-arrow" aria-label="Next" role="button" style="display: block;">Next</button></div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				
				<!-- archi why us -->

				<section class="archi-whyus">
					<div class="container">
						<div class="row">
							<div class="col-xs-12">
								<div class="text-box">
									<h2>Why to choose <span class="add">SOOMA</span>?</h2>
									<p><?php echo $aboutus[0]->data;?></p>
									<a class="btn btn-history" href="#">About Us</a>
								</div>
								<!-- img-box -->
								<div class="img-box fadeInRight animated" data-animate="fadeInRight" data-delay="200">
									<img src="<?php echo base_url();?>Assets/assets/build.jpg" alt="image description" class="img-responsive">
								</div>
							</div>
						</div>
					</div>
				</section>
			</main>
		<!-- footer of the page -->
			<footer id="footer" class="style14">
				<!-- socialize holder -->
				<div class="socialize-holder">
					<div class="container">
						<div class="row">
							<div class="col-xs-12">
								<strong class="title">Socialize with us</strong>
								<!-- footer-social -->
								<ul class="list-inline footer-social">
									<li class="facebook"><a href="<?php echo $fb[0]->data;?>"><i class="fa fa-facebook"></i></a></li>
									<li class="twitter"><a href="#"><i class="fa fa-twitter"></i></a></li>
									<li class="google-plus"><a href="#"><i class="fa fa-google-plus"></i></a></li>
									
								</ul>
							</div>
						</div>
					</div>
					<!-- parallax-holder -->
					<div class="parallax-holder">
						<div class="parallax-frame" style="padding-bottom: 720px; background-image: url(&quot;<?php echo base_url();?>Assets/assets/Bg1.jpg&quot;); background-attachment: fixed; background-size: 1899px 1186.88px; background-position: 50% -133.938px; background-repeat: no-repeat;"><img src="<?php echo base_url();?>Assets/assets/img08.jpg" height="1200" width="1920" alt="image description" style="visibility: hidden;"></div>
					</div>
				</div>
				<!-- footer cent -->
				<div id="contact" class="footer-cent bg-shark">
					<div class="container">
						<div class="row">
							<div class="col-xs-12 col-sm-6 col-md-3 info-box column1">
								<!-- f contact info2 -->
								<div class="f-contact-info2">
									<h5>Contact Information</h5>
									<address><i class="fa fa-map-marker"></i> <?php echo $location[0]->data;?> </address>
									<div class="mail-box2">
										<span class="mail-box"><i class="fa fa-envelope-o"></i> <a href="mailto:info@sooma.ae" class="email"> <?php echo $email[0]->data;?></a></span>
									</div>
									<div class="tel-holder">
										<span class="tel-box"><i class="fa fa-phone"></i> <a href="tel:00201008431112" class="tel"><?php echo $phone[0]->data;?></a></span>
									</div>
									<div class="mail-box2">
										<span class="mail-box"><i class="fa fa-link"></i> <a href="mailto:www.sooma.ae" class="email">sooma.ae</a></span>
									</div>
								</div>
							</div>
							<div class="col-xs-12 col-sm-6 col-md-3 column2">
								<!-- f video box -->
								<div class="f-video-box">
									<h5>Company Portfolio</h5>
									<div class="video-area">
										<div class="fluid-width-video-wrapper" style="padding-top: 56.2%;">
											<!--<iframe   src="<?php echo base_url();?>Assets/assets/DubaiFL.mp4" frameborder="0" webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen="" id="fitvid465872"></iframe>-->
										</div>

									</div>
								</div>
							</div>
							<div class="col-xs-12 col-sm-6 col-md-3 column3 clearfix-sm">
								<!-- f mailing form2 -->
								<div class="f-mailing-form2">
									<h5>Mailing List</h5>
									<div class="mailing-form2">
										<label>Please enter your email address for our mailing list to keep your self our lastest updated.</label>
										<div class="form-col">
											<!-- Begin MailChimp Signup Form -->
											<div id="mc_embed_signup">
												<form action="#" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate="">
													<div id="mc_embed_signup_scroll">
														<div class="mc-field-group">
															<label for="mce-EMAIL">Email Address </label>
															<input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" placeholder="Email Address">
														</div>
														<div id="mce-responses" class="clear">
															<div class="response" id="mce-error-response" style="display:none"></div>
															<div class="response" id="mce-success-response" style="display:none"></div>
														</div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
														<div style="position: absolute; left: -5000px;"><input type="text" name="b_cb2d5a07fdf0d86c96f260674_1103b14a3b" tabindex="-1" value=""></div>
														<div class="clear">
															<button type="submit" name="subscribe" id="mc-embedded-subscribe" class="btn button btn-submit"><i class="fa fa-paper-plane"></i></button>
														</div>
													</div>
												</form>
											</div>
											<!--End mc_embed_signup-->
										</div>
									</div>
								</div>
							</div>
							<div class="col-xs-12 col-sm-6 col-md-3 column4">
								<!-- f flicker -->
							
									<img src="<?php echo base_url();?>Assets/assets/LogoC.png"  alt="Sooma" style="width:100%;"	>
								
							</div>
						</div>
					</div>
				</div>
				<!-- footer bottom -->
				<div class="footer-bottom bg-dark-jungle">
					<div class="container">
						<div class="row">
							<div class="col-xs-12">
								<div class="bottom-box1">
									<!-- footer nav -->
									<ul class="list-inline footer-nav">
										<li><a href="#">Home</a></li>
										<li><a href="#">About Us</a></li>
										<li><a href="#">Careers</a></li>
										<li><a href="#">Privacy Policy</a></li>
										<li><a href="#">Use of terms</a></li>
									</ul>
									<span class="copyright">© 2016 Developed by <a href="#"> Overlimits LLC</a></span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</footer>
		</div>
		<div class="fa fa-chevron-up" id="gotoTop" style="display: none;"></div>
	</div>
	
	<!-- include jQuery library -->
	<div class="fit-vids-style" id="fit-vids-style" style="display: none;">­
		<style>.fluid-width-video-wrapper{width:100%;position:relative;padding:0;}
		.fluid-width-video-wrapper iframe,.fluid-width-video-wrapper object,.fluid-width-video-wrapper embed {position:absolute;top:0;left:0;width:100%;height:100%;}</style></div>

		<script type="text/javascript" src="<?php echo base_url();?>Assets/assets/jquery-1.11.3.min.js"></script>
	<!-- include bootstrap JavaScript -->
	<script type="text/javascript" src="<?php echo base_url();?>Assets/assets/bootstrap.min.js"></script>
	<!-- include custom JavaScript -->
	<script type="text/javascript" src="<?php echo base_url();?>Assets/assets/jquery.main.js"></script>
	<!-- include plugins JavaScript -->
	<script type="text/javascript" src="<?php echo base_url();?>Assets/assets/plugins.js"></script>

</body>
</html>
